{
  lib,
  pkgs,
  config,
  ...
}: let
  rootDir = ../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;

  gitUrl = "https://gitlab.com/sceptri-docs/tarre.git";
  rootLocation = "/var/pages/";
  pageLocation = "tarre";
  playerPageLocation = "players-tarre";
  renderedLocation = "/_book";

  cacheLocation = "/var/empty";

  tmpLocation = "/tmp/" + pageLocation;
  location = rootLocation + pageLocation;
  siteLocation = location + renderedLocation;

  playerLocation = rootLocation + playerPageLocation;
  playerSiteLocation = playerLocation + renderedLocation;

  sitePackages = [];
in {
  systemd.services.tarre-page = {
    description = "deploy of the Tarre page";

    requires = ["network.target"];

    environment = {
      DENO_DIR = tmpLocation;
      XDG_CACHE_HOME = tmpLocation;
    };

    script = ''
       test -d ${location} && rm -rf ${location}
       test -d ${playerLocation} && rm -rf ${playerLocation}

       git clone -c credential.helper= -c credential.helper='!f() { echo username=${secrets.git.username}; echo "password=${secrets.git.password}"; };f' ${gitUrl} ${location}
       git clone -c credential.helper= -c credential.helper='!f() { echo username=${secrets.git.username}; echo "password=${secrets.git.password}"; };f' ${gitUrl} ${playerLocation}

      # Render DM version
       cd ${location}
       chmod -R a+rwx ${location}

       echo "Rendering..."
       # TODO: PDF version looks like crap
       quarto render --to html

       chmod -R a+rwx ${siteLocation}

      # Render Players version
       cd ${playerLocation}
       chmod -R a+rwx ${playerLocation}

       echo "Rendering player version..."
       quarto render --to html --profile players

       chmod -R a+rwx ${playerSiteLocation}
    '';

    path = config.environment.systemPackages ++ config.users.users.quarto.packages ++ sitePackages;

    serviceConfig = {
      Type = "simple";
      PermissionsStartOnly = true;
      RemainAfterExit = false;
      WorkingDirectory = rootLocation;
      User = "quarto";
      RuntimeMaxSec = "infinity";
      Restart = "on-failure";
    };
  };

  services.nginx.virtualHosts = {
    "tarre.zapadlo.name" = {
      forceSSL = true;
      enableACME = true;

      root = siteLocation;

      extraConfig = ''
        error_page 401 = @error401;
      '';

      locations = {
        "/" = {
          extraConfig = ''
            auth_request /sso-auth;
            auth_request_set $cookie $upstream_http_set_cookie;
            add_header Set-Cookie $cookie;
          '';

          root = siteLocation;
        };

        "/logout" = {
          return = "302 https://sso.zapadlo.name/logout?go=$scheme://$http_host/";
        };

        "/sso-auth" = {
          extraConfig = ''
            internal;

            proxy_pass_request_body off;
            proxy_set_header Content-Length "";

            proxy_set_header X-Origin-URI $request_uri;
            proxy_set_header X-Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Application "Tarre";

          '';

          proxyPass = "https://sso.zapadlo.name/auth";
        };

        "@error401" = {
          return = "302 https://sso.zapadlo.name/login?go=$scheme://$http_host$request_uri";
        };
      };
    };

    "dnd.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      root = playerSiteLocation;
    };
  };
}
