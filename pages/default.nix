{
  lib,
  config,
  pkgs,
  ...
}: let
  rootDir = ../.;
  unstablePkgs = import <nixos-unstable> {};

  args = {
    inherit
      config
      lib
      pkgs
      ;
  };

  quartoPreRelease = pkgs.callPackage (rootDir + "/packages/quarto/preRelease.nix") (with pkgs lib config; args);
in {
  imports = [
    ./profile.nix
    ./tarre.nix
    ./uni/courses/m9121_time_series.nix
    ./uni/courses/ia168_game_theory.nix
    ./uni/courses/m8110_pde.nix
    ./uni/dashboard.nix
    ./uni/misc/m9121_time_series_project.nix
    ./uni/misc/git_good_workshop.nix
    ./uni/courses/m9bcf_interactive.nix
    ./uni/thesis.nix
  ];

  users = {
    groups.quarto = {};
    groups.pages = {};

    users.quarto = {
      isSystemUser = true;

      group = "quarto";
      extraGroups = ["wheel" "root" "pages"];

      packages = [
        quartoPreRelease
        pkgs.git
      ];

      createHome = true;
      homeMode = "777";
      home = "/home/quarto";
    };
  };

  systemd.tmpfiles.rules = [
    "d  /var/pages  0777  quarto  pages"
    "d  /var/pages/uni  0777  quarto  pages"
    "d  /var/pages/uni/courses  0777  quarto  pages"
    "d  /var/pages/uni/misc  0777  quarto  pages"
  ];
}
