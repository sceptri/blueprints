{
  lib,
  pkgs,
  config,
  ...
}: let
  rootDir = ../../../.;
  args = {};

	pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;

  R-env = pkgs.rWrapper.override {
    packages = with pkgs.rPackages; [
      zoo
      forecast
      aod
      readxl
    ];
  };

  unstablePkgs = import <nixos-unstable> {};
in let
  gitUrl = "https://gitlab.com/sceptri-university/courses/m9121-time-series.git";
  rootLocation = "/var/pages/";
  pageLocation = "uni/misc/m9121_time_series";
  renderedLocation = "/project";

  cacheLocation = "/var/empty";
  tmpLocation = "/tmp/" + pageLocation;
  location = rootLocation + pageLocation;
  siteLocation = location + renderedLocation;
  renderAtLocation = location + "/project";
  renderTarget = "project.qmd";

  sitePackages = with pkgs; [R-env pandoc texlive.combined.scheme-full gnumake];
in {
  systemd.services.m9121-project-page = {
    description = "deploy of the M9121 Time Series Project page";
    enable = false; # INFO: [MS1-14] Until quarto correctly uses supplied R libraries, this cannot be used

    requires = ["network.target"];
    wantedBy = ["nginx.service"];

    environment = {
      DENO_DIR = tmpLocation;
      XDG_CACHE_HOME = tmpLocation;
    };

    script = ''
      which R

      test -d ${location} && rm -rf ${location}
      git clone -c credential.helper= -c credential.helper='!f() { echo username=${secrets.git.username}; echo "password=${secrets.git.password}"; };f' ${gitUrl} ${location}

      cd ${location}
      chmod -R a+rwx ${location}

      cd ${renderAtLocation}

      echo "Rendering..."
      echo $(quarto check)
      # TODO: [WB-10] Latex not yet installed
      quarto render ${renderTarget} --to all

      chmod -R a+rwx ${renderAtLocation}

      test -d ${tmpLocation} && rm -rf ${tmpLocation}
    '';

    path = sitePackages ++ config.environment.systemPackages; #++ config.users.users.quarto.packages;

    serviceConfig = {
      Type = "simple";
      PermissionsStartOnly = true;
      RemainAfterExit = false;
      WorkingDirectory = rootLocation;
      User = "quarto";
      RuntimeMaxSec = "infinity";
      Restart = "on-failure";
    };
  };

  services.nginx.virtualHosts = {
    "m9121-project.uni.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      root = siteLocation;
    };
  };
}
