{
  lib,
  pkgs,
  config,
  ...
}: let
  rootDir = ../../../.;
  args = {inherit config lib pkgs;};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;

  catterplots = pkgs.rPackages.buildRPackage rec {
    name = "CatterPlots-${version}";
    version = "0.0.2";
    requireX = false;
    src = pkgs.fetchFromGitHub {
      owner = "Gibbsdavidl";
      repo = "CatterPlots";
      rev = "ae17cd5e49ddda4ecfe0eba8a4c21df8c88e72c4";
      sha256 = "sha256-z3dSw/MWjhDK4KRNNSzEn23dSlbysY5ee/06R32kSGE=";
    };
    buildInputs = with pkgs.rPackages; [pkgs.R devtools png];
  };

  rSet = with pkgs.rPackages; [
    quarto
    devtools
    png
    languageserver
    markdown
    catterplots
  ];

  quartoPreRelease = extraRPackages:
    pkgs.callPackage (rootDir + "/packages/quarto/preRelease.nix")
    (with pkgs lib config; {extraRPackages = extraRPackages;});

  unstablePkgs = import <nixos-unstable> {};

  gitUrl = "https://gitlab.com/sceptri-university/git-good-workshop.git";
  rootLocation = "/var/pages/";
  pageLocation = "uni/misc/git_good_workshop";

  cacheLocation = "/var/empty";
  tmpLocation = "/tmp/" + pageLocation;
  location = rootLocation + pageLocation;

  renderedFilename = "presentation";

  sitePackages = with pkgs; [
    (rWrapper.override {packages = rSet;})
    (quartoPreRelease rSet)
    pandoc
    texlive.combined.scheme-full
    gnumake
  ];
in {
  systemd.services.git-good-workshop-presentation = {
    description = "deploy of the Git good workshop presentation";

    requires = ["network.target"];
    wantedBy = ["nginx.service"];

    environment = {
      DENO_DIR = tmpLocation;
      XDG_CACHE_HOME = tmpLocation;
    };

    script = ''
      test -d ${location} && rm -rf ${location}
      git clone -c credential.helper= -c credential.helper='!f() { echo username=${secrets.git.username}; echo "password=${secrets.git.password}"; };f' ${gitUrl} ${location}

      cd ${location}
      chmod -R a+rwx ${location}

      echo "Rendering..."
      echo $(quarto check)
      quarto add jmbuhr/quarto-qrcode --no-prompt
      quarto render -o ${renderedFilename}.html

      chmod -R a+rwx ${location}

      test -d ${tmpLocation} && rm -rf ${tmpLocation}
    '';

    path = sitePackages ++ config.environment.systemPackages;
    restartIfChanged = true;

    serviceConfig = {
      Type = "simple";
      PermissionsStartOnly = true;
      RemainAfterExit = false;
      WorkingDirectory = rootLocation;
      User = "quarto";
      RuntimeMaxSec = "infinity";
      Restart = "on-failure";
      RestartSec = 5;
    };

    unitConfig = {
      StartLimitInterval = 30;
      StartLimitBurst = 5;
    };
  };

  services.nginx.virtualHosts = {
    "git-good-workshop.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      root = location;

      locations = {
        "/" = {
          index = "${renderedFilename}.html ${renderedFilename}-speaker.html working_script.R";
        };

        "/edit" = {
          return = "301 https://demo.hedgedoc.org/66aEjXhAR3aZovdt2L1bBQ?both#";
        };
      };
    };
  };
}
