{
  lib,
  pkgs,
  config,
  ...
}: let
  rootDir = ../../.;
  args = {};

	pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;

  gitUrl = "https://gitlab.com/sceptri-docs/dashboard-uni.git";
  rootLocation = "/var/pages/";
  pageLocation = "dashboard-uni";
  renderedLocation = "/_site";

  cacheLocation = "/var/empty";
  tmpLocation = "/tmp/" + pageLocation;
  location = rootLocation + pageLocation;
  siteLocation = location + renderedLocation;

  sitePackages = [
    pkgs.texlive.combined.scheme-full
    pkgs.pandoc
    pkgs.R
    pkgs.librsvg
    pkgs.rPackages.rsvg
  ];
in {
  systemd.services.dashboard-uni-page = {
    description = "deploy of the Dashboard | University page";

    requires = ["network.target"];

    environment = {
      DENO_DIR = tmpLocation;
      XDG_CACHE_HOME = tmpLocation;
    };

    script = ''
      test -d ${location} && rm -rf ${location}
      git clone -c credential.helper= -c credential.helper='!f() { echo username=${secrets.git.username}; echo "password=${secrets.git.password}"; };f' ${gitUrl} ${location}

      cd ${location}
      chmod -R a+rwx ${location}

      echo "Rendering..."
      quarto render

      chmod -R a+rwx ${siteLocation}

      test -d ${tmpLocation} && rm -rf ${tmpLocation}
    '';

    path = config.environment.systemPackages ++ config.users.users.quarto.packages ++ sitePackages;

    serviceConfig = {
      Type = "simple";
      PermissionsStartOnly = true;
      RemainAfterExit = false;
      WorkingDirectory = rootLocation;
      User = "quarto";
      RuntimeMaxSec = "infinity";
      Restart = "on-failure";
    };
  };

  services.nginx.virtualHosts = {
    "uni.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      root = siteLocation;
    };
  };
}
