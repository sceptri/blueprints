{
  lib,
  pkgs,
  config,
  ...
}:
with lib; let
  rootDir = ../../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));

  secrets = importConfig "secrets" "general" args;
  kestraSecrets = importConfig "secrets" "kestra" args;

  rootLocation = "/var/pages/";
  pageLocation = "uni/thesis";
  renderedLocation = "/_book";

  cacheLocation = "/var/empty";
  tmpLocation = "/tmp/" + pageLocation;
  location = rootLocation + pageLocation;
  siteLocation = location + renderedLocation;

  renderAtLocation = location + "/presentation";
  renderTarget = "presentation";
in {
  systemd.services.thesis-page = {
    description = "Deploy Thesis page using Kestra";

    requires = ["network.target" "kestra.service"];

    environment = {
      DENO_DIR = tmpLocation;
      XDG_CACHE_HOME = tmpLocation;
      NIX_PATH = builtins.getEnv "NIX_PATH";
      NIXOS_CONFIGURATION_DIR = builtins.getEnv "NIXOS_CONFIGURATION_DIR";
    };

    path = config.environment.systemPackages ++ config.users.users.quarto.packages;

    serviceConfig = {
      Type = "simple";
      PermissionsStartOnly = true;
      RemainAfterExit = false;
      WorkingDirectory = rootLocation;
      
      User = "quarto";
      Group = "quarto";
      
      RuntimeMaxSec = "infinity";
      Restart = "on-failure";
    };
    
    script = ''
      curl https://kestra.zapadlo.name/api/v1/executions/webhook/pages/thesis-restart/${kestraSecrets.webhooks.THESIS_GIT_WEBHOOK} \
        -H "Accept: application/json" \
        -H "Authorization: Token ${secrets.sso.tokens."general-api"}"
    '';
  };

  services.nginx.virtualHosts = {
    "thesis.uni.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      root = siteLocation;

      # locations = {
      #   "/presentation" = {
      #     index = "${renderTarget}.html";
      #   };
      # };
    };
  };
}
