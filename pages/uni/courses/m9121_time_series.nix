{
  lib,
  pkgs,
  config,
  ...
}: let
  rootDir = ../../../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;

  gitUrl = "https://gitlab.com/sceptri-university/courses/m9121-time-series.git";
  rootLocation = "/var/pages/";
  pageLocation = "uni/courses/m9121_time_series";
  renderedLocation = "/_book";
	
  cacheLocation = "/var/empty";
  tmpLocation = "/tmp/" + pageLocation;
  location = rootLocation + pageLocation;
  siteLocation = location + renderedLocation;

  # TODO: Somehow use the shell.nix instead of this crap
  quartoPreRelease = extraRPackages:
    pkgs.callPackage (rootDir + "/packages/quarto/preRelease.nix")
    (with pkgs lib config; {extraRPackages = extraRPackages;});

  rPackages = with pkgs.rPackages; [
    quarto
    aod
    readxl
    forecast
    zoo
    rmarkdown
    languageserver
    astsa
    TSA
    fma
  ];

  sitePackages = [
    pkgs.texlive.combined.scheme-full
    pkgs.pandoc
    (
      pkgs.rWrapper.override {
        packages = rPackages;
      }
    )
    (
      quartoPreRelease rPackages
    )
    pkgs.librsvg
    pkgs.fontconfig
    pkgs.git
    pkgs.chromium # INFO: Needed for quarto
  ];
in {
  systemd.services.m9121-page = {
    description = "deploy of the M9121 Time Series page";

    requires = ["network.target"];

    environment = {
      DENO_DIR = tmpLocation;
      XDG_CACHE_HOME = tmpLocation;
    };

    script = ''
      test -d ${location} && rm -rf ${location}
      git clone -c credential.helper= -c credential.helper='!f() { echo username=${secrets.git.username}; echo "password=${secrets.git.password}"; };f' ${gitUrl} ${location}

      cd ${location}
      chmod -R a+rwx ${location}

      echo "Rendering..."
      quarto render --to all

      chmod -R a+rwx ${siteLocation}

      test -d ${tmpLocation} && rm -rf ${tmpLocation}
    '';

    path = config.environment.systemPackages ++ sitePackages;

    serviceConfig = {
      Type = "simple";
      PermissionsStartOnly = true;
      RemainAfterExit = false;
      WorkingDirectory = rootLocation;
      User = "quarto";
      RuntimeMaxSec = "infinity";
      Restart = "on-failure";
    };
  };

  services.nginx.virtualHosts = {
    "m9121.uni.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      root = siteLocation;
    };
  };
}
