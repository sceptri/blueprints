{
  lib,
  pkgs,
  config,
  ...
}: let
  rootDir = ../../../.;
  args = {};

  unstablePkgs = import <nixos-unstable> {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));

  secrets = importConfig "secrets" "general" args;

  gitUrl = "https://gitlab.com/sceptri-university/courses/m9bcf-bifurcations-chaos-and-fractals.git";
  rootLocation = "/var/pages/";
  pageLocation = "uni/courses/m9bcf_interactive";

  cacheLocation = "/var/empty";
  tmpLocation = "/tmp/" + pageLocation;
  location = rootLocation + pageLocation;

  sitePackages = [];

  plutoDeployment = {
    SliderServer = {
      port = 7766;
      host = "0.0.0.0";

      watch_dir = false;
    };
  };
	deployDirectory = "public/.";

  configFilename = "PlutoDeployment.toml";
  serverEnvironment = "pluto-slider-server-environment";

  format = pkgs.formats.toml {};
  configFile = format.generate configFilename plutoDeployment;
in {
  systemd.services.m9bcf-page = {
    description = "deploy of the M9BCF Bifurcations, Chaos and Fractals page";

    requires = ["network.target"];

    environment = {
      DENO_DIR = tmpLocation;
      XDG_CACHE_HOME = tmpLocation;
      JULIA_PKG_USE_CLI_GIT = "true";
    };

    script = ''
      test -d ${location} && rm -rf ${location}
      git clone -c credential.helper= -c credential.helper='!f() { echo username=${secrets.git.username}; echo "password=${secrets.git.password}"; };f' ${gitUrl} ${location}

      cd ${location}

      mkdir ${serverEnvironment}
      cp ${configFile} ${serverEnvironment}/${configFilename}

      chmod -R a+rwx ${location}

      julia --project=${serverEnvironment} -e "import Pkg; Pkg.add(\"PlutoSliderServer\"); import PlutoSliderServer; PlutoSliderServer.run_directory(\"${deployDirectory}\")"
    '';

    path = config.environment.systemPackages ++ sitePackages;

    serviceConfig = {
      Type = "simple";
      PermissionsStartOnly = true;
      RemainAfterExit = false;
      WorkingDirectory = rootLocation;
      User = "quarto";
      RuntimeMaxSec = "infinity";
      Restart = "on-failure";
    };
  };

  services.nginx.virtualHosts = {
    "m9bcf.uni.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      extraConfig = ''
        proxy_set_header Host              $host;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host  $host;
      '';

      locations."/".proxyPass = "http://localhost:${toString plutoDeployment.SliderServer.port}";
    };
  };
}
