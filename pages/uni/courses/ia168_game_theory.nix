{
  lib,
  pkgs,
  config,
  ...
}: let
  rootDir = ../../../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));

  secrets = importConfig "secrets" "general" args;

  gitUrl = "https://gitlab.com/sceptri-university/courses/ia168-game-theory.git";
  rootLocation = "/var/pages/";
  pageLocation = "uni/courses/ia168_game_theory";
  renderedLocation = "/_book";

  cacheLocation = "/var/empty";
  tmpLocation = "/tmp/" + pageLocation;
  location = rootLocation + pageLocation;
  siteLocation = location + renderedLocation;

  sitePackages = [
    pkgs.texlive.combined.scheme-full
    pkgs.pandoc
    pkgs.R
    pkgs.librsvg
    pkgs.rPackages.rsvg
  ];
in {
  systemd.services.ia168-page = {
    description = "deploy of the IA168 Algorithmic Game Theory page";

    requires = ["network.target"];

    environment = {
      DENO_DIR = tmpLocation;
      XDG_CACHE_HOME = tmpLocation;
    };

    script = ''
      test -d ${location} && rm -rf ${location}
      git clone -c credential.helper= -c credential.helper='!f() { echo username=${secrets.git.username}; echo "password=${secrets.git.password}"; };f' ${gitUrl} ${location}

      cd ${location}
      chmod -R a+rwx ${location}

      echo "Rendering..."
      quarto render --to all

      chmod -R a+rwx ${siteLocation}

      test -d ${tmpLocation} && rm -rf ${tmpLocation}
    '';

    path = config.environment.systemPackages ++ config.users.users.quarto.packages ++ sitePackages;

    serviceConfig = {
      Type = "simple";
      PermissionsStartOnly = true;
      RemainAfterExit = false;
      WorkingDirectory = rootLocation;
      User = "quarto";
      RuntimeMaxSec = "infinity";
      Restart = "on-failure";
    };
  };

  services.nginx.virtualHosts = {
    "ia168.uni.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      root = siteLocation;
    };
  };
}
