{
  lib,
  pkgs,
  config,
  ...
}:
with lib; let
  rootDir = ../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;

  gitUrl = "https://gitlab.com/sceptri-docs/profile.git";
  rootLocation = "/var/pages/";
  pageLocation = "profile";
  renderedLocation = "/_site";

  cacheLocation = "/var/empty";
  tmpLocation = "/tmp/" + pageLocation;
  location = rootLocation + pageLocation;
  siteLocation = location + renderedLocation;

  sitePackages = [
    pkgs.julia
    pkgs.gnumake
    pkgs.nix
  ];

  scriptName = "deployScript";
  scriptFile =
    pkgs.writeScriptBin scriptName
    ''
      #! /usr/bin/env nix-shell
      #! nix-shell -i bash ${location}/shell.nix
      # TODO: This must be changed later, as it will fail if it already does NOT exist

      test -d ${location} && rm -rf ${location}
      git clone -c credential.helper= -c credential.helper='!f() { echo username=${secrets.git.username}; echo "password=${secrets.git.password}"; };f' ${gitUrl} ${location}

      cd ${location}
      chmod -R a+rwx ${location}

      echo "Current Julia version"
      echo $(julia -v)

      julia --project="${location}/julia_env" -e "using Pkg; Pkg.instantiate()"
      julia --project="${location}" -e "using Pkg; Pkg.instantiate()"

      echo "Rendering..."
      quarto render

      chmod -R a+rwx ${siteLocation}

      test -d ${tmpLocation} && rm -rf ${tmpLocation}
    '';
in {
  systemd.services.profile-page = {
    description = "deploy of the Profile page";

    requires = ["network.target"];

    environment = {
      DENO_DIR = tmpLocation;
      XDG_CACHE_HOME = tmpLocation;
      NIX_PATH = builtins.getEnv "NIX_PATH";
      NIXOS_CONFIGURATION_DIR = builtins.getEnv "NIXOS_CONFIGURATION_DIR";
    };

    path = config.environment.systemPackages ++ config.users.users.quarto.packages ++ sitePackages;

    serviceConfig = {
      Type = "simple";
      PermissionsStartOnly = true;
      RemainAfterExit = false;
      WorkingDirectory = rootLocation;
      User = "quarto";
      Group = "quarto";
      RuntimeMaxSec = "infinity";
      Restart = "on-failure";
      ExecStart = "${scriptFile}/bin/${scriptName}";
    };
  };

  services.nginx.virtualHosts = {
    "stepa.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      root = siteLocation;
    };
  };
}
