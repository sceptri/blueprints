{
  lib,
  stdenvNoCC,
  fetchzip,
  ...
}: let
  fullVersion = {
    main = 4;
    sub = 1;
  };
  underVer = "${toString fullVersion.main}_${toString fullVersion.sub}";
in
  stdenvNoCC.mkDerivation rec {
    pname = "bellota";
    version = "${toString fullVersion.main}.${toString fullVersion.sub}";

    src = fetchzip {
      url = "https://github.com/kemie/Bellota-Font/releases/download/v${version}/Bellota_${underVer}.zip";
      hash = "sha256-Vdx7j9Gd6KZePd89JX1hzUagEawee2KMwQwsBciShQo=";
      stripRoot = false;
    };

    installPhase = ''
      runHook preInstall

      install -m644 -Dt $out/share/fonts/opentype otf/*.otf

      runHook postInstall
    '';

    meta = with lib; {
      description = "Bellota font";
      longDescription = ''
        Bellota is an ornamented, low contrast sans-serif with text and swash alternates. It's just cute enough!
      '';
      homepage = "https://github.com/kemie/Bellota-Font";
      platforms = platforms.all;
    };
  }
