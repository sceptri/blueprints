{
  lib,
  pkgs ? import <nixpkgs> {},
  version ? {
    breaking = 0;
    major = 19;
    minor = 10;
    hash = "12dlk1g28ncz76zjfmqy6n1lnff4wxiwldl0l7hrh9yzmnz8vgri"; # v0.19.10
  },
}:
with pkgs; let
  kestraVersion = "${toString version.breaking}.${toString version.major}.${toString version.minor}";

  additionalPackages = [
    systemd
    libudev-zero
    libudev0-shim
    udev
  ];

  selectedJdk = jdk;
  architecture = "linux-x86-64";
in
  stdenv.mkDerivation {
    name = "kestra";
    src = fetchurl {
      url = "https://github.com/kestra-io/kestra/releases/download/v${kestraVersion}/kestra-${kestraVersion}";
      sha256 = version.hash;
    };

    nativeBuildInputs = [makeWrapper selectedJdk ant] ++ additionalPackages;
    buildInputs = [makeWrapper coreutils selectedJdk ant] ++ additionalPackages;
    phases = ["installPhase" "postFixup"];

    installPhase = ''
      mkdir -p $out/bin
      cp $src $out/bin/kestra
      chmod +x $out/bin/kestra
    '';

    postFixup = ''
      wrapProgram $out/bin/kestra \
      --prefix PATH : ${lib.makeBinPath ([
          coreutils
          ant
          selectedJdk # INFO: Sending mail stopped working for some reason
        ]
        ++ additionalPackages)} \
      --prefix LD_LIBRARY_PATH : ${libudev-zero}/lib \
      --argv0 $out/bin/.kestra-wrapped
    '';
  }
