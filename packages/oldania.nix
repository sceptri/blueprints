{
  lib,
  stdenvNoCC,
  fetchurl,
  ...
}:
stdenvNoCC.mkDerivation rec {
  pname = "oldania";
  version = "20150406";

  src = fetchurl {
    url = "https://arkandis.tuxfamily.org/fonts/Oldania-Std-${version}.tar.gz";
    hash = "sha256-L98OVujzqUCDJb+fkE82tWHONbENTrNcHw7z9pyGevI=";
  };

  installPhase = ''
    runHook preInstall

    install -m644 -Dt $out/share/fonts/opentype OTF/*.otf

    runHook postInstall
  '';

  meta = with lib; {
    description = "Oldania font";
    longDescription = ''
      Oldania a fantasy looking font
    '';
    homepage = "https://arkandis.tuxfamily.org/adffonts.html";
    platforms = platforms.all;
  };
}
