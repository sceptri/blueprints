#WARNING: In progress
{
  lib,
  pkgs ? import <nixpkgs> {},
}:
with pkgs;
  stdenv.mkDerivation rec {
    pname = "xp-pen-star-g640s-driver";
    version = "1936";

    src = fetchzip {
      url = "https://www.xp-pen.com/download/file.html?id=${version}&pid=58&ext=gz";
      sha256 = "1r423hcpi26v82pzl59br1zw5vablikclqsy6mcqi0v5p84hfrdd";
    };

    nativeBuildInputs = [
      autoPatchelfHook
    ];

    buildInputs = [
      libusb1
      libX11
      libXtst
      qtbase
      libglvnd
      stdenv.cc.cc.lib
    ];

    installPhase = ''
      mkdir -p $out/bin
      cp Pentablet_Driver $out/bin/pentablet-driver
      cp config.xml $out/bin/config.xml
    '';

    meta = with lib; {
      homepage = "https://www.xp-pen.com/download-46.html";
      description = "Driver for XP-PEN Pentablet drawing tablets";
      sourceProvenance = with lib.sourceTypes; [binaryNativeCode];
      license = licenses.unfree;
      platforms = ["x86_64-linux"];
      maintainers = with maintainers; [ivar];
    };
  }
