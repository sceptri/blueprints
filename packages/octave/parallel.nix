# FIXME: Does NOT seem to work
{
  buildOctavePackage,
  lib,
  fetchurl,
  struct,
  gnutls,
  pkg-config,
  ...
}:
buildOctavePackage rec {
  pname = "parallel";
  version = "4.0.2";

  src = fetchurl {
    url = "mirror://sourceforge/octave/${pname}-${version}.tar.gz";
    sha256 = "02rj6gfn8cdrhircd8r8ly4dpm1dhi6sjr31c9ghdy1d9vm0h0qs";
  };
  # patches = [
  #   ./c_verror.patch
  # ];

  nativeBuildInputs = [
    pkg-config
  ];

  buildInputs = [
    gnutls
  ];

  propagatedBuildInputs = [
    gnutls
  ];

  requiredOctavePackages = [
    struct
  ];

  meta = with lib; {
    homepage = "https://octave.sourceforge.io/parallel/index.html";
    license = licenses.gpl3Plus;
    maintainers = with maintainers; [KarlJoad];
    description = "Parallel execution package";
  };
}
