{pkgs ? import <nixpkgs> {}}:
pkgs.stdenv.mkDerivation {
  name = "dagu";
  src = pkgs.fetchurl {
    url = "https://github.com/yohamta/dagu/releases/download/v1.9.3/dagu_1.9.3_Linux_x86_64.tar.gz";
    sha256 = "ImNNbR1Mc0oUwyvgIMZAtpryKiqo9yu3QwiHtxwirW0=";
  };
  phases = ["installPhase" "patchPhase"];
  installPhase = ''
    mkdir -p $out/bin
    cp $src $out/bin/daguTar
    tar -xvzf $out/bin/daguTar --directory $out/bin
    chmod +x $out/bin/dagu
  '';
}
