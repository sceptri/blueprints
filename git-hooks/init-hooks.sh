# INFO: It is necessary to symlink git-hooks to .git/hooks and mark them executable
ln -s $NIXOS_CONFIGURATION_DIR/git-hooks/* $NIXOS_CONFIGURATION_DIR/.git/hooks/
chmod u+x .git/hooks/*