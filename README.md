# NixOS Configuration *Blueprints*

> This configuration was previously managed [under a private repository](https://gitlab.com/sceptri/homelab) mainly due to the inclusion of secrets in plain text config files.
> 
> Right now, hopefully, all sensitive info should be encrypted using [git-secret](https://sobolevn.me/git-secret/). 
> The old repo couldn't be used as there was too much info in the git history to be really redactable.


This is my NixOS configuration for

- my homelab (read *server*) which runs on ~~Acer Chromebook CB3-431~~ ThinkPad T440s (i7-4600U & 8 GB of RAM)
- my workstation which runs on ThinkPad T490s (i7-8565U & 16 GB of RAM)

> As for naming scheme, storage units are named after celestial bodies (Mars, Pluto, ...) and servers are named after ? 
<!-- TODO: What are servers named after? -->

## Structure

This config is split into multiple folders:

- `hosts`
  - `workstation` - workstation (i.e. my laptop) specific config using Home Manager including VPN and Eduroam, GNOME extensions, VS Code config etc.
  - `homelab` - homelab (i.e. server) specific configuration including wifi config and more
- `apps` - user-facing services hosted on the homelab, for example, Memos, Vikunja, Bookstack, Kestra and many more
- `services` - backend files for both homelab and workstation, for example, Zabbix, Mosquitto along with GNOME config and autostart
- `programs` - configuration of certain programs (which are available under `prograns.<name>`), for example, VS Code, Flatpaks and Julia & R with package declarations
- `modules` - Nix modules that I created, e.g. Kestra, Dagu, Esphome (my own version inspired by the "official" version) and Flood
- `packages` - Nix packages (and fonts)
- `pages` - configuration of pages that are hosted on homelab along with "recipes" to build them
- `lib` - contains type configuration for flatpak
- `flatpak` - flatpak Nix module adapted from [declarative-flatpak](https://github.com/GermanBread/declarative-flatpak)
- `secrets` - contains secrets that are present in the config, but should not be public (**though they are still world-readable in `/nix/store`**) - implemented via [git-secret](https://sobolevn.me/git-secret/)
- `git-hooks` - git hooks (most importantly pre-commit, post-commit & post-merge) used for encrypting/decrypting secrets
- `server-keys` - secret/public GPG keys used with [git-secret](https://sobolevn.me/git-secret/)

## Deploying

Should an entirely new configuration be necessary, it will be necessary to create it in the `hosts` folder. To use an existing config, it is necessary to symlink the appropriate `hosts` subfolder to `etc/nixos`, e.g.
```bash
ln -s /home/sceptrix/Documents/Dev/homelab/hosts/homelab/configuration.nix /etc/nixos/configuration.nix
```

Also, depending on the configuration, several channels are necessary to add (e.g. for homelab)
```bash
sudo nix-channel --add https://channels.nixos.org/nixos-23.11 nixos
sudo nix-channel --add https://channels.nixos.org/nixos-unstable nixos-unstable
sudo nix-channel --update
```

In any case, instructions to get the config up and running should be in the appropriate `configuration.nix` file.

> To answer the question of why don't I use flakes... I don't know how XD!
>
> But a transition to flakes is planned at some point.

## Attributions & License

The Blueprint icon [![Blueprint icon](design/blueprint.png){width=20 height=20}](https://www.flaticon.com/free-icons/blueprint) is created & provided by [Freepik Flaticon](https://www.flaticon.com/free-icons/blueprint).

The rest of this repository is available under the MIT license.