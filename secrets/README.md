# Git secret

## Secret folders
- `secrets`
- `server-keys`

## Git Hooks
Only `pre-commit` and `post-commit` are "authentic". The hooks `post-checkout` and `post-merge` are only symlinks of `post-commit`.

## Deploying new machine

1. create new GPG secret/public keys and add them to the `server-keys` folder
2. add them using `git secret tell`
3. commit & push & pull
4. set the necessary env variables, e.g.
   1. `export MACHINE_IDENTIFIER=homelab`
   2. `export CONFIG_PASSPHRASE=`
   3. `export CONFIG_SECRET_ID=`
5. copy over the appropriate private key using sftp or similar
6. run the contents of the `post-commit` script