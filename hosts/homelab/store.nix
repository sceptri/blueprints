{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}: {
  # INFO: Limit of generations in the /boot partition (otherwise it can break rebuild switch)
  boot.loader.systemd-boot.configurationLimit = 5;

  # INFO: Store optimisation saves space...
  nix.optimise = {
    automatic = true;
    dates = ["03:00"];
  };

  nix.settings.auto-optimise-store = true;

  nix.gc = {
    automatic = true;
    dates = "daily";
    options = "--delete-older-than 5d";
  };
}
