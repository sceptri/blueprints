# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
# INFO: Used channels (after adding all the channels, please run: sudo nix-channel --update):
# INFO: Actually, this command could be run more often
# sudo nix-channel --add https://channels.nixos.org/nixos-24.11 nixos
# sudo nix-channel --add https://channels.nixos.org/nixos-unstable nixos-unstable
# INFO: Symlink to this configuration similarly to this:
# ln -s /home/sceptrix/Documents/Dev/homelab/hosts/homelab/configuration.nix /etc/nixos/configuration.nix
{
  lib,
  config,
  pkgs,
  callPackage,
  ...
}: let
  rootDir = ../../.;

  args = {
    inherit
      config
      lib
      pkgs
      ;
  };

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  getModule = object: (pathTo object "default");
  importConfig = type: (name: (args: (import (pathTo type name) args)));

  kestra = pkgs.callPackage (pathTo "packages" "kestra") {};

  unstablePkgs = import <nixos-unstable> {};
  secrets = importConfig "secrets" "general" args;
in {
  imports = [
    # Include the results of the hardware scan.
    <nixos-hardware/lenovo/thinkpad/t440s>
    ./hardware/thinkpad-hardware-configuration.nix
    ./parts/default.nix
    ./cachix.nix

    ./store.nix
    ./git.nix

    (getModule "services")
    (getModule "apps")
    (getModule "pages")
  ];

  users.groups.homeshare = {
    gid = 12345;
  };

  users.users.sceptrix = {
    isNormalUser = true;
    home = "/home/sceptrix";
    homeMode = "755"; # INFO: systemd services need to load the configuration
    description = "Sceptri Main";
    extraGroups = ["wheel" "networkmanager" "homeshare"];
    hashedPassword = secrets.linux.user;
  };

  users.users.root.hashedPassword = secrets.linux.root;
  users.mutableUsers = false;

  environment.sessionVariables = {
    MACHINE_IDENTIFIER = "homelab";
    NIXOS_CONFIGURATION_DIR = "${config.users.users.sceptrix.home}/Documents/Dev/homelab";
    CONFIG_PASSPHRASE = secrets.git-secret.homelab-pass;
    CONFIG_SECRET_ID = secrets.git-secret.homelab-user;
  };

  users.users.root.extraGroups = ["pages"];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  security.acme = {
    acceptTerms = true;
    defaults.email = "admin+stepan.zapadlo@gmail.com";
  };

  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = [
    # editors
    pkgs.helix

    # commonly used
    pkgs.ncdu
    pkgs.yazi

    # dev stuff
    pkgs.git
    pkgs.git-secret
    kestra
    pkgs.gnumake

    # video
    pkgs.ocl-icd

    # languages
    pkgs.php83
    (unstablePkgs.symlinkJoin {
      name = "julia";
      paths = [unstablePkgs.julia];
      buildInputs = [unstablePkgs.makeWrapper];
      postBuild = ''
        wrapProgram $out/bin/julia \
        	--set LD_LIBRARY_PATH /run/opengl-driver/lib/:${lib.makeLibraryPath [unstablePkgs.stdenv.cc.cc.lib]}
      '';
    })
    # pkgs.python310Packages.jupyter-core
    # pkgs.python310Packages.notebook
    # pkgs.python310Packages.jupyter-client

    # misc
    pkgs.epiphany
    pkgs.procps

    # jellyfin
    unstablePkgs.jellyfin
    unstablePkgs.jellyfin-web
    unstablePkgs.jellyfin-ffmpeg
  ];

  services.pcscd.enable = true;
  services.dbus.packages = [pkgs.gcr];
  programs.gnupg.agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-curses;
    enableSSHSupport = true;
  };

  services.openiscsi = {
    enable = true; # Enable openiscsi daemon
    name = "iqn.2024-09.com.nixos:my-nixos-initiator";

    discoverPortal = "192.168.0.3";
  };

  # [HL-83] Add autoupdates
  system.autoUpgrade.enable = true;

  # [HL-52] Necessary for reloading wireguard via Kestra
  security.polkit.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Prague";

  fonts.packages = with pkgs; [
    victor-mono
    gyre-fonts
    fira-code
    fira-code-symbols
    dejavu_fonts
    lmodern
    libertinus
  ];

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    # keyMap = "cz-qwerty";
    useXkbConfig = true; # use xkbOptions in tty.
  };

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    desktopManager = {
      xterm.enable = true;
      xfce.enable = true;
    };
  };
  services.displayManager.defaultSession = "xfce";

  # Configure keymap in X11
  services.xserver.xkb.layout = "cz";
  services.xserver.xkb.variant = "qwerty";
  # services.xserver.xkbOptions = {
  #   "eurosign:e";
  #   "caps:escape" # map caps to escape.
  # };

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  hardware.pulseaudio.enable = false;

  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.users.jane = {
  #   isNormalUser = true;
  #   extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
  #   packages = with pkgs; [
  #     firefox
  #     thunderbird
  #   ];
  # };

  services.nginx.enable = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  # environment.systemPackages = with pkgs; [
  #   vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #   wget
  # ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}
