{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}: {
  programs.git = {
    enable = true;
    config = {
      user = {
        name = "Štěpán Zapadlo";
        email = "stepan.zapadlo@gmail.com";
      };
    };
  };
}
