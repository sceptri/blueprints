{
  config,
  lib,
  pkgs,
  ...
}: let
  unstablePkgs = import <nixos-unstable> {};
in {
  services.mediamtx = {
    enable = false;
    allowVideoAccess = true;
    settings = {
      logDestinations = ["stdout"];
      logFile = "/var/log/mediamtx/mediamtx.log";
      logLevel = "info";

      paths = {
        cam = {
          runOnInit = "${lib.getExe unstablePkgs.ffmpeg-full} -f v4l2 -i /dev/video0 -f rtsp -rtsp_transport tcp -map 0 -c:v libx264 -pix_fmt yuv420p -preset veryfast -profile:v main -crf 25 rtsp://192.168.0.2:8554/cam";
          runOnInitRestart = true;
        };
      };
    };
  };
}
