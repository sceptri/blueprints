{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}: {
  users.users.share = {
    group = "homeshare";
    isSystemUser = true;
    createHome = false;
  };

  # services.samba = {
  #   enable = false;
  #   securityType = "user";
  #   openFirewall = true;
  #   settings = {
  #     "workgroup" = "WORKGROUP";
  #     "server string" = "smbnix";
  #     "netbios name" = "smbnix";
  #     "security" = "user";
  #     #use sendfile = yes
  #     #max protocol = smb2
  #     # note: localhost is the ipv6 localhost ::1
  #     "hosts allow" = ["192.168.0." "192.168.1." "127.0.0.1" "localhost"];
  #     "hosts deny" = ["0.0.0.0/0"];
  #     "guest account" = "nobody";
  #     "map to guest" = "bad user";
	# 	};
  #   shares = {
  #     public = {
  #       path = "/drives/";
  #       browseable = "yes";
  #       "read only" = "no";
  #       "guest ok" = "yes";
  #       "create mask" = "0644";
  #       "directory mask" = "0755";
  #       "force user" = "share";
  #       "force group" = "homeshare";
  #     };
  #   };
  # };
}
