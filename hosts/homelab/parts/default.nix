{
  lib,
  config,
  pkgs,
  ...
}: {
  imports = [
    ./video-recording.nix
    ./networking.nix
    ./sharing.nix
  ];
}
