{
  lib,
  config,
  pkgs,
  ...
}: let
  rootDir = ../../../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;
in {
  networking.hostName = "homelab"; # Define your hostname.
  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  # networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default

  networking = {
    useDHCP = false;

    wireless = {
      enable = true;

      networks = {
        "Vodafone-730C" = {
          psk = secrets.wifi.flat;
        };
      };
    };

    defaultGateway = {
      address = "192.168.0.1";
      interface = "wlp3s0";
    };

    defaultGateway6 = {
      address = "fe80::4aa9:8aff:fe7e:ea2e";
      interface = "wlp3s0";
    };

    nameservers = [
      "8.8.8.8"
      "10.10.10.1"
    ];

    interfaces.wlp3s0.ipv4.addresses = [
      {
        address = "192.168.0.2";
        prefixLength = 24;
      }
    ];

    interfaces.wlp3s0.ipv6.addresses = [
      {
        address = "2a01:510:d504:5c00:0000:0000:0000:0001";
        prefixLength = 64;
      }
    ];
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ 80 3456 ];
  # networking.firewall.allowedUDPPorts = [ 13231 ];
  # Or disable the firewall altogether.
  networking.firewall = let
    web_ports = [80 443];
    zabbix_ports = [10050];
    mqtt_ports = [1883];
    esphome_ports = [6052];
    other_ports = [8096];
  in {
    enable = true;
    allowedTCPPorts = web_ports ++ zabbix_ports ++ other_ports ++ mqtt_ports ++ esphome_ports;
  };
}
