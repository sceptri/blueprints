# INFO: Home Manager installation with
# https://nix-community.github.io/home-manager/index.html#ch-installation
# CAUTION: Please read https://github.com/nix-community/home-manager#words-of-warning
{
  lib,
  config,
  pkgs,
  ...
}: {
  home-manager.users.sceptri = import ./sceptri.nix;
  # home-manager.backupFileExtension = "backup"; # When home-manager goes bad...
}
