# INFO: Even though I tried, I couldn't get Gnome Online Accounts
# and anything Evolution-related to declaratively set up
# Maybe one can ~/.config/goa-1.0/accounts.conf or similar for evolution
{
  config,
  lib,
  pkgs,
  ...
}: let
  rootDir = ../../../.;
  programs = rootDir + "/programs/.";

  args = {
    inherit
      config
      lib
      pkgs
      ;
  };

  importConfig = type: (name: (args: (import (rootDir + "/${type}/." + "/${name}.nix") args)));
  importProgram = importConfig "/programs/.";
  importPackageSet = importConfig "/programs/sets/.";

  gnomeConfig = importConfig "services" "gnome-extensions" args;
  juliaSet = importPackageSet "julia" args;
  rSet = importPackageSet "r" args;

  unstablePkgs = import <nixos-unstable> {};

  # I decided to use ltex instread
  # valeSet = importPackageSet "vale" args;
  # valeConfigFile = "~/.config/.vale.ini";
  # Both ltex and vale suck...
  # old-ltex-ls = pkgs.callPackage (rootDir + "/packages/ltex-ls-old.nix") (with pkgs lib config; args);
  # INFO: Example of a wrapper
  # wrapped-ltex-ls = pkgs.writeShellScriptBin "ltex-ls" ''
  #   exec ${old-ltex-ls}/bin/ltex-ls $@ --log-file ltex.log
  # '';

  vscodeConfig = importProgram "vscode" args;

  quartoPreRelease = pkgs.callPackage (rootDir + "/packages/quarto/preRelease.nix") (with pkgs lib config; args);

  newCrystal = pkgs.callPackage (rootDir + "/packages/crystal/default.nix") (with pkgs lib config; args);
  octaveParallel = pkgs.callPackage (rootDir + "/packages/octave/parallel.nix") (with unstablePkgs lib config; {
    inherit lib config;
    buildOctavePackage = unstablePkgs.octavePackages.buildOctavePackage;
    struct = unstablePkgs.octavePackages.struct;
  });
  # sweetHome = pkgs.callPackage (rootDir + "/packages/sweethome3d.nix") (with pkgs lib config; {inherit lib;});
in {
  nixpkgs.overlays = import (programs + "/matlab.nix");
  # Nix User Repository (NUR)
  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
      inherit pkgs;
    };
  };
  nixpkgs.config.allowUnfree = true;

  nixpkgs.config.permittedInsecurePackages = [
    "python3.11-youtube-dl-2021.12.17"
  ];

  home.packages = with pkgs;
    [
      # Day-to-day stuff
      nextcloud-client
      evolution
      keepassxc
      homebank
      foliate
      wordbook
      pdfarranger
      transmission_4-gtk
      chromium # INFO: Needed also for Quarto
      khronos
      unstablePkgs.mousam
      unstablePkgs.mission-center
      libreoffice
      varia
      # Uni
      xournalpp
      write_stylus
      netlogo
      unstablePkgs.hieroglyphic
      (unstablePkgs.octaveFull.withPackages (
        ps: with ps; [struct octaveParallel]
      ))
      # citations
      setzer
      # Image Stuff
      gimp
      inkscape
      krita
      #mypaint
      # Video Stuff
      clapper
      shotcut
      vlc
      # Music
      spotify
      lollypop
      # Messaging
      unstablePkgs.simplex-chat-desktop
      discord
      slack
      zoom-us
      #unstablePkgs.cinny-desktop
      # fluffychat
      # unstablePkgs.teams-for-linux # Even on ustable, it works like crap
      # Game Engines
      godot_4
      # Notes
      joplin-desktop
      # Data Science
      quartoPreRelease
      matlab
      # Languages
      (unstablePkgs.maven.override {
        jdk_headless = unstablePkgs.jdk21;
      })
      unstablePkgs.jdk21
      #crystal
      #newCrystal
      unstablePkgs.crystal
      crystalline
      unstablePkgs.zig
      # Crystalline needs also
      pkg-config
      ameba
      shards
      php
      mariadb
      postgresql
      # Development
      toybox
      git
      # unstablePkgs.datalad
      git-secret
      gitg
      gparted # sudo -E gparted
      gnome-builder
      dbeaver-bin
      gnumake
      guake
      cachix # Needed for devenv
      # INFO: It was necessary to run: cachix use devenv
      (import (fetchTarball https://install.devenv.sh/latest)).default
      # Dependencies & Misc
      texliveFull
      librsvg
      home-manager
      dconf-editor
      xclip # needed for image pasting to VSCode
      # old-ltex-ls
      # Fonts
      victor-mono
      xits-math
      # Spell Checking
      hunspellDicts.cs_CZ
      hunspellDicts.cs-cz
      hunspellDicts.en_US-large
      hunspellDicts.en_GB-large
    ]
    ++ gnomeConfig.packages
    ++ juliaSet # Julia is not working at the moment
    ++ rSet;
  #++ valeSet.packages;

  dconf.settings = gnomeConfig.dconf;

  home.file =
    (importConfig "services" "autostart" {
      inherit pkgs lib config;
      autostartPrograms = with pkgs; [
        nextcloud-client
        keepassxc
        # unstablePkgs.teams-for-linux
        evolution
      ];
    })
    .autostart;

  # INFO: Necessary for writable user settings https://github.com/nix-community/home-manager/issues/1800
  # Same treatise as for user settings implemented for keybindings
  home.activation.boforeCheckLinkTargets = {
    after = [];
    before = ["checkLinkTargets"];
    data = ''
      userDir=~/.config/VSCodium/User
      rm -rf $userDir/settings.json
      rm -rf $userDir/keybindings.json
    '';
    # Now not-used vale config
    # rm -f ${valeConfigFile}
  };

  home.activation.afterWriteBoundary = let
    userSettings = vscodeConfig.userSettings;
    keybindings = vscodeConfig.keybindings;
  in {
    after = ["writeBoundary"];
    before = [];
    data = ''
      userDir=~/.config/VSCodium/User
      rm -rf $userDir/settings.json
      cat ${pkgs.writeText "tmp_vscode_settings" (builtins.toJSON userSettings)} | jq --monochrome-output > $userDir/settings.json
      rm -rf $userDir/keybindings.json
      cat ${pkgs.writeText "tmp_vscode_keybindings" (builtins.toJSON keybindings)} | jq --monochrome-output > $userDir/keybindings.json
    '';
    # Now not-used vale config
    # rm -f ${valeConfigFile}
    # cat ${pkgs.writeText "tmp_vale_ini" (lib.generators.toINIWithGlobalSection {} valeSet.config)} > ${valeConfigFile}
  };

  programs = {
    vscode = vscodeConfig;
    firefox = importProgram "firefox" args;
    git = importProgram "git" args;
    java = {
      enable = true;
      # On change remember to clear vscode java language server workspace
      package = unstablePkgs.jdk21;
    };
  };

  services = {
    nextcloud-client = {
      enable = true;
      startInBackground = true;
    };
    kdeconnect = {
      enable = true;
      indicator = true;
    };
  };

  xdg.configFile."mimeapps.list".force = true;

  # Desktop entries can be found by: ls -l /run/current-system/sw/share/applications/
  xdg.mimeApps = let
    browser = ["firefox.desktop"];
    # textProcessor = ["writer.desktop"];
    # spreadsheet = ["impress.desktop"];
    torrent = ["transmission-gtk.desktop"];
    textEditor = ["org.gnome.TextEditor.desktop"];
    imageViewer = ["org.gnome.Loupe.desktop"];
    archiveTool = ["org.gnome.fileRoller.desktop"];
    documentViewer = ["org.gnome.Evince.desktop"];

    appAssociations = {
      "text/html" = browser;
      "application/x-bittorrent" = torrent;
      "x-scheme-handler/http" = browser;
      "x-scheme-handler/https" = browser;
      "x-scheme-handler/about" = browser;
      "x-scheme-handler/unknown" = browser;
      # "application/vnd.openxmlformats-officedocument.wordprocessingml.document" = textProcessor;
      # "application/msword" = textProcessor;
      # "application/vnd.oasis.opendocument.text" = textProcessor;
      # "text/csv" = spreadsheet;
      # "application/vnd.oasis.opendocument.spreadsheet" = spreadsheet;
      "text/plain" = textEditor;
      "image/*" = imageViewer;
      "image/png" = imageViewer;
      "image/jpg" = imageViewer;
      "image/jpeg" = imageViewer;
      "application/zip" = archiveTool;
      "application/rar" = archiveTool;
      "application/7z" = archiveTool;
      "application/*tar" = archiveTool;
      "application/pdf" = documentViewer;
    };
  in {
    enable = true;
    defaultApplications = appAssociations;
    associations.added = appAssociations;
  };

  home.stateVersion = "23.05";
}
