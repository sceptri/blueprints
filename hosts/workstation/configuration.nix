# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
# INFO: Used channels (after adding all the channels, please run: sudo nix-channel --update):
# INFO: Actually, this command could be run more often
# sudo nix-channel --add https://channels.nixos.org/nixos-24.11 nixos
# sudo nix-channel --add https://github.com/nix-community/home-manager/archive/release-24.11.tar.gz home-manager
# sudo nix-channel --add https://channels.nixos.org/nixos-unstable nixos-unstable
# INFO: Symlink to this configuration similarly to this:
# ln -s /home/sceptri/Documents/Dev/homelab/hosts/workstation/configuration.nix /etc/nixos/configuration.nix
{
  config,
  pkgs,
  lib,
  ...
}: let
  rootDir = ../../.;
  args = {inherit config lib pkgs;};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  getModule = object: (pathTo object "default");
  importConfig = type: (name: (args: (import (pathTo type name) args)));

  secrets = importConfig "secrets" "general" args;
  unstablePkgs = import <nixos-unstable> {};
in {
  imports = [
    # Include the results of the hardware scan.
		<nixos-hardware/lenovo/thinkpad/t490s>
    ./hardware-configuration.nix
    <home-manager/nixos>
    ./home/default.nix

    # Copied the current stable source of declarative-flatpak
    ../../flatpak/modules/nixos.nix

    # INFO: Avahi must run on both client and server (it did not work)
    # (pathTo "apps" "avahi")

    ./cachix.nix

    # Implements store optimisation and garbage collect
    ./store.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Kernel configuration
  boot.kernelPackages = pkgs.linuxPackages_latest;
  services.fwupd.enable = true;

  networking.hostName = "workstation"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Prague";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8"; # Czech locale is cs_CZ.UTF-8

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_GB.UTF-8";
    LC_IDENTIFICATION = "en_GB.UTF-8";
    LC_MEASUREMENT = "en_GB.UTF-8";
    LC_MONETARY = "en_GB.UTF-8";
    LC_NAME = "en_GB.UTF-8";
    LC_NUMERIC = "en_GB.UTF-8";
    LC_PAPER = "en_GB.UTF-8";
    LC_TELEPHONE = "en_GB.UTF-8";
    LC_TIME = "en_GB.UTF-8";
  };

  hardware.graphics.enable = true;
  hardware.graphics.extraPackages = with pkgs; [
    mesa
  ];

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the GNOME Desktop Environment.
  services.xserver.displayManager.gdm = {
    enable = true;
    # INFO: the suspend issue seem to be caused by the external disk, so I won't deal with them right now
    autoSuspend = true;
    wayland = true;
    debug = true;
  };
  services.xserver.desktopManager.gnome.enable = true;
  # INFO: Needed for correct functioning of matlab installer
  # services.xserver.desktopManager.xfce.enable = true;

  # Configure keymap in X11
  services.xserver = {
    xkb.layout = "cz";
    xkb.variant = "qwerty";
    videoDrivers = [
      "displayLink" # INFO: needed for USB-C dock external display
      "fbdev" # defaults
      "modesetting" #defaults
    ];
  };

  # Configure console keymap
  console.keyMap = "cz-lat2";

  # Enable CUPS to print documents.
  services.printing.enable = true;
  hardware.sane = {
    enable = true;
    extraBackends = [pkgs.hplipWithPlugin];
  };
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    publish = {
      enable = true;
      addresses = true;
      userServices = true;
    };
  };

  # Security
  security.rtkit.enable = true;
  # INFO: Otherwise Gparted does not launch
  security.polkit.enable = true;

  # Enable sound with pipewire.
  # sound.enable = true;
  hardware.pulseaudio.enable = false;
  hardware.enableAllFirmware = true;

  # Merge connections and pipewire configs
  environment.etc = lib.mkMerge [
    (importConfig "utils" "connections" args)
    {
      "wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
        bluez_monitor.properties = {
        	["bluez5.enable-sbc-xq"] = true,
        	["bluez5.enable-msbc"] = true,
        	["bluez5.enable-hw-volume"] = true,
        	["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
        }
      '';
    }
  ];

  environment.sessionVariables = {
    MACHINE_IDENTIFIER = "workstation";
    NIXOS_CONFIGURATION_DIR = "${config.users.users.sceptri.home}/Documents/Dev/homelab";
    CONFIG_PASSPHRASE = secrets.git-secret.workstation-pass;
    CONFIG_SECRET_ID = secrets.git-secret.workstation-user;
  };

  fonts.packages = with pkgs; [
    victor-mono
    gyre-fonts
    fira-code
    fira-code-symbols
    dejavu_fonts
    lmodern
    libertinus
    (pkgs.callPackage (rootDir + "/packages/oldania.nix") (with lib; args))
    (pkgs.callPackage (rootDir + "/packages/bellota.nix") (with lib; args))
  ];

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    wireplumber.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # ------- NOTES ------
  # NOTE: Guide on how include/do LaTeX documents:
  # https://flyx.org/nix-flakes-latex/

  # TODO: Nextcloud declarative sync
  # Setup per https://nixos.wiki/wiki/Nextcloud#Nextcloudcmd
  # ------- END OF NOTES -------

  users.groups.homeshare = {
    gid = 12345;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.sceptri = {
    isNormalUser = true;
    description = "Štěpán Zapadlo";
    extraGroups = ["networkmanager" "wheel" "homeshare"];
    hashedPassword = secrets.linux.user;
  };

  users.users.root.hashedPassword = secrets.linux.root;
  users.mutableUsers = false;

  nix.settings.trusted-users = ["root" "sceptri"];

  services.flatpak = {
    enable = true;
    enableModule = true;
    packages = importConfig "programs" "flatpaks" args;
    remotes = {
      "flathub" = "https://flathub.org/repo/flathub.flatpakrepo";
      "flathub-beta" = "https://flathub.org/beta-repo/flathub-beta.flatpakrepo";
    };
  };

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  programs.kdeconnect = {
    enable = true;
    package = pkgs.gnomeExtensions.gsconnect;
  };

  services.gvfs.enable = true;

  # TODO: Move to programs/firefox.nix
  programs.firefox.nativeMessagingHosts = {
    gsconnect = true;
    packages = [
      pkgs.gnome-browser-connector
    ];
  };

  services.pcscd.enable = true;
  services.dbus.packages = [pkgs.gcr];
  programs.gnupg.agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-curses;
    enableSSHSupport = true;
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    helix
    adw-gtk3
    openvpn
    procps
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
}
