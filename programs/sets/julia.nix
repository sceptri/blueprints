{lib, ...}: let
  unstablePkgs = import <nixos-unstable> {};
in [
  # FIXME: Right now, it does not build
  # (unstablePkgs.julia-bin.withPackages
  #   [
  #     "Plots"
  #     "Makie"
  #     "DrWatson"
  #     "Revise"
  #     "LinearAlgebra"
  #     "Statistics"
  #   ])
  # Without this fix, GLFW (and by extension GLMakie) does not work
  # See https://discourse.julialang.org/t/glmakie-and-nixos/111897
  # and https://github.com/JuliaGL/GLFW.jl/issues/194
  # and https://nixos.wiki/wiki/Nix_Cookbook#Wrapping_packages
  # Also needed to add std.cc.cc.lib, otherwise libquadmath.so.0 is missing for Flux
  (unstablePkgs.symlinkJoin {
    name = "julia";
    paths = [unstablePkgs.julia];
    buildInputs = [unstablePkgs.makeWrapper];
    postBuild = ''
      wrapProgram $out/bin/julia \
      	--set LD_LIBRARY_PATH /run/opengl-driver/lib/:${lib.makeLibraryPath [unstablePkgs.stdenv.cc.cc.lib]}
    '';
  })
]
