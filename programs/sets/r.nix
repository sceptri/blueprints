# INFO: Configure R using
# https://nixos.wiki/wiki/R
{pkgs, ...}: let
  rSet = with pkgs.rPackages; [
    forecast
    ggplot2
    languageserver
    markdown
    rmarkdown
    quarto
    devtools
    png # Needed for Git Good Workshop Catter plots example
  ];
in [
  (pkgs.rWrapper.override {
    packages = rSet;
  })
  (pkgs.rstudioWrapper.override {
    packages = rSet;
  })
]
