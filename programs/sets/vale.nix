{pkgs, ...}: let
  unstablePkgs = import <nixos-unstable> {};
in {
  packages = [
    (unstablePkgs.vale.withStyles (
      s:
        with pkgs.valeStyles; [
          alex
          google
          write-good
          readability
          proselint
          microsoft
          joblint
        ]
    ))
    unstablePkgs.vale-ls
  ];

  config = {
    globalSection.MinAlertLevel = "suggestion";
    #sections.Packages = ["RedHat" "proselint" "write-good" "alex" "Readability" "Joblint"];

    sections."*" = {
      BasedOnStyles = builtins.concatStringsSep "," [
        "Vale"
        "Google"
        "proselint"
        "write-good"
        "alex"
        "Readability"
        "Joblint"
      ];
    };
  };
}
