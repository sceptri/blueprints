{...}: {
  enable = true;
  userName = "Štěpán Zapadlo";
  userEmail = "stepan.zapadlo@gmail.com";
  extraConfig = {
    pull.rebase = false;
  };
}
