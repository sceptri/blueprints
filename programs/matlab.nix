# CAUTION: MATLAB does NOT work for su, as it doesn't not have correctly defined $HOME
# TODO: Installing matlab per
# https://gitlab.com/doronbehar/nix-matlab
# subdirectory ~/
# INFO: The config is ~/.config/matlab/nix.sh
# INFO: License file is in ~/Documents/Uni/Licences/network.lic
let
  # TODO: Eventually change back to doronbehar/nix-matlab!
  nix-matlab = import (builtins.fetchTarball "https://gitlab.com/sceptri/nix-matlab/-/archive/master/nix-matlab-master.tar.gz");
in [
  nix-matlab.overlay
  (
    final: prev: {
      # Your own overlays...
    }
  )
]
