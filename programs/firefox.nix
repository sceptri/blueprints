{pkgs, ...}: 
let
	unstablePkgs = import <nixos-unstable> {};
in {
  enable = true;
	package = unstablePkgs.firefox;
  #enableGnomeExtensions = true;

  profiles."sceptri" = {
    extensions = with pkgs.nur.repos.rycee.firefox-addons; [
      ublock-origin
      keepassxc-browser
      grammarly
      tab-session-manager
      improved-tube
      gsconnect
      video-downloadhelper
      darkreader
      momentumdash
      bitwarden
      startpage-private-search
      copy-link-text
      hoppscotch
			leechblock-ng
			passbolt
    ];

    isDefault = true;
    name = "Štěpán Zapadlo";

    settings = {
      # Performance settings
      "gfx.webrender.all" = true; # Force enable GPU acceleration
      "media.ffmpeg.vaapi.enabled" = true;
      "widget.dmabuf.force-enabled" = true; # Required in recent Firefoxes

      "extensions.pocket.enabled" = false;
    };
  };
}
