{pkgs, ...}: let
  system = builtins.currentSystem;
  extensions =
    (import (builtins.fetchGit {
      url = "https://github.com/nix-community/nix-vscode-extensions";
      ref = "refs/heads/master";
      # WARNING: Might be necessary to update sometimes to include all extensions
      rev = "487e99ffa42d57de53eba5ca4b60cd95fb442c42"; # Update on 24.9.2024
    }))
    .extensions
    .${system};

  unstablePkgs = import <nixos-unstable> {};
in {
  enable = true;
  enableExtensionUpdateCheck = true;
  mutableExtensionsDir = false;

  package = unstablePkgs.vscodium;

  # TODO: Fix nix-env-selector with https://www.jetpack.io/blog/how-we-got-around-vscodes-env-variable-restriction/
  # INFO: Unavailable extensions are pulled from https://github.com/nix-community/nix-vscode-extensions
  extensions = with extensions.vscode-marketplace;
    [
      aliariff.auto-add-brackets
      jrbentzon.bibtex-formatter
      crystal-lang-tools.crystal-lang
      veelenga.crystal-ameba
      streetsidesoftware.code-spell-checker-czech
      hediet.vscode-drawio
      # kaiwood.endwise
      # guidotapia2.unicode-math-vscode # Works like shit
      rafaelmardojai.vscode-gnome-theme
      # znck.grammarly # Grammarly seems to have been abandoned and removed
      # valentjn.vscode-ltex # Did not manage to make it working, sadly
      ban.spellright
      julialang.language-julia
      kestra-io.kestra
      tecosaur.latex-utilities
      stephendolan.lucky
      goessner.mdmath
      #gimly81.matlab
      webfreak.debug
      wjmccann.xpp
      mushan.vscode-paste-image
      xdebug.php-debug
      quarto.quarto
      reditorsupport.r
      humao.rest-client
      rust-lang.rust-analyzer
      natizyskunk.sftp
      # whatwedo.twig
      oijaz.unicode-latex
      octref.vetur
      evilz.vscode-reveal
      mathworks.language-matlab
      #eamodio.gitlens
      waderyan.gitblame
      donjayamanne.githistory
      ziglang.vscode-zig
      # INFO: I don't need then anymore (needed for PV021 project)
      # redhat.java
      # vscjava.vscode-maven
      # vscjava.vscode-java-debug
      # vscjava.vscode-java-test
      # vscjava.vscode-java-dependency
      snakemake.snakemake-lang
    ]
    ++ (
      with extensions.open-vsx; [
        #chrischinchilla.vale-vscode
      ]
    )
    ++ (
      with pkgs.vscode-extensions; [
        # Using Nixpkgs extensions if available
        bbenoist.nix
        kamadorueda.alejandra
        redhat.vscode-xml
        redhat.vscode-yaml
        piousdeer.adwaita-theme
        gruntfuggly.todo-tree
        bmewburn.vscode-intelephense-client
        ms-python.vscode-pylance
        ms-python.python
        ms-vscode.makefile-tools
        yzhang.markdown-all-in-one
        james-yu.latex-workshop
        ms-toolsai.jupyter
        ms-toolsai.vscode-jupyter-cell-tags
        ms-toolsai.jupyter-keymap
        ms-toolsai.jupyter-renderers
        ms-toolsai.vscode-jupyter-slideshow
        golang.go
        tamasfe.even-better-toml
        grapecity.gc-excelviewer
        ms-vscode.cpptools
        streetsidesoftware.code-spell-checker
        arrterian.nix-env-selector
      ]
    );

  # INFO: User settings are writeable (see sceptri.nix)
  # see https://discourse.nixos.org/t/vscode-extensions-setup/1801
  userSettings = {
    # File Watcher
    "search.exclude" = {
      "**/.history" = true;
    };
    "files.exclude" = {
      "**/.history" = true;
    };
    "files.watcherExclude" = {
      "**/.history/**" = true;
    };

    # Theming
    "workbench.preferredDarkColorTheme" = "Adwaita Dark & default syntax highlighting & colorful status bar";
    "workbench.preferredHighContrastColorTheme" = "Adwaita Dark & default syntax highlighting & colorful status bar";
    "workbench.preferredHighContrastLightColorTheme" = "Adwaita Light & default syntax highlighting & colorful status bar";
    "workbench.preferredLightColorTheme" = "Adwaita Light & default syntax highlighting & colorful status bar";
    "window.titleBarStyle" = "custom";
    "window.autoDetectColorScheme" = true;

    # Font
    "editor.fontFamily" = "Fira Code";
    "editor.fontLigatures" = true;

    # Editor
    "editor.insertSpaces" = false;
    "editor.detectIndentation" = false;
    "editor.unicodeHighlight.allowedCharacters" = {
      "ρ" = true;
      "σ" = true;
      "γ" = true;
      "α" = true;
    };
    "editor.minimap.enabled" = false;
    "files.autoSave" = "afterDelay";
    "editor.formatOnPaste" = true;

    # Git
    "git.confirmSync" = false;
    "git.autofetch" = true;

    "gitblame.inlineMessageEnabled" = true;
    "gitblame.inlineMessageFormat" = "∵ \${author.name} (\${time.ago}) | \${commit.summary}";
    "gitblame.inlineMessageMargin" = 0.5;
    # "gitlens.autolinks" = [
    #   {
    #     "ignoreCase" = false;
    #   }
    # ];

    # Spelling
    "cSpell.language" = "en,cs";
    "cSpell.enableFiletypes" = [
      "quarto"
      "juliamarkdown"
    ];
    "grammarly.files.include" = [
      "**/readme.md"
      "**/README.md"
      "**/*.txt"
      "**/*.tex"
      "**/*.md"
      "**/*.qmd"
    ];

    # Julia
    "julia.execution.inlineResultsForCellEvaluation" = true;
    "julia.symbolCacheDownload" = true;
    "julia.enableTelemetry" = true;
    "julia.lint.run" = true;
    "terminal.integrated.commandsToSkipShell" = [
      "language-julia.interrupt"
    ];

    # Quarto
    "[quarto]" = {
      "editor.wordWrap" = "on";
      "editor.quickSuggestions" = {
        "comments" = "on";
        "other" = "on";
        "strings" = "on";
      };
      "editor.quickSuggestionsDelay" = 100;
      "editor.wordBasedSuggestions" = false;
      "editor.suggestOnTriggerCharacters" = true;
      "editor.unicodeHighlight.ambiguousCharacters" = false;
      "editor.unicodeHighlight.invisibleCharacters" = false;
      "editor.snippets.codeActions.enabled" = true;
      "editor.snippetSuggestions" = "inline";
    };

    # Markdown
    "markdownShortcuts.icons.citations" = true;
    "markdown.math.enabled" = false;

    # Latex
    "latex-workshop.latex.autoBuild.run" = "never";
    "latex-workshop.intellisense.citation.backend" = "biblatex";
    "latex-workshop.intellisense.atSuggestion.user" = {
      "@jl" = "\\begin{juliablock}$1\\end{juliablock}";
      "@jlc" = "\\begin{juliaconsole}$1\\end{juliaconsole}";
      "@jlv" = "\\juliav|$1|";
      "@tb" = "\\textbf{$1}";
      "@ti" = "\\textit{$1}";
    };
    "latex-workshop.latex.verbatimEnvs" = [
      "verbatim"
      "lstlisting"
      "minted"
      "juliaconsole"
      "juliaverbatim"
      "juliacode"
    ];
    "latex-workshop.latex.external.build.command" = "make";
    "latex-workshop.latex.external.build.args" = [
      "auto-compile"
      "ARGS=%DOCFILE%"
    ];

    # Bibtex
    "[bibtex]" = {
      "editor.defaultFormatter" = "James-Yu.latex-workshop";
    };

    # Paste Image
    "pasteImage.path" = "\${currentFileDir}/images/pastebin";
    "pasteImage.insertPattern" = "![](\${imageSyntaxPrefix}\${imageFilePath}\${imageSyntaxSuffix})";

    # DrawIO
    "hediet.vscode-drawio.resizeImages" = null;

    # Matlab
    "MATLAB.installPath" = "/home/sceptri/Applications/Matlab";

    # LTeX spell checking
    # "ltex.ltex-ls.path" = "/home/sceptri/.nix-profile/";
    # "ltex.java.path" = "/home/sceptri/.nix-profile/bin/";

    # Crystal
    "crystal-lang.server" = "/home/sceptri/.nix-profile/bin/crystalline";

    # Java
    "java.jdt.ls.java.home" = "/home/sceptri/.nix-profile";

    # Julia persistent session error: https://github.com/julia-vscode/julia-vscode/issues/3145
    # "terminal.integrated.enablePersistentSessions" = false;
    # TODO: Probably remove, did not seem to do much...
  };

  globalSnippets = {
    quarto_gathered = {
      scope = "markdown, quarto";
      prefix = ["eqgat"];
      body = [
        "$$"
        "\\begin{\${2:gathered}}"
        "$1"
        "\\end{\${2:gathered}}"
        "$$ $3"
      ];
    };

    quarto_align = {
      scope = "markdown, quarto";
      prefix = ["eqalign"];
      body = [
        "\\begin{\${2:align*}}"
        "$1"
        "\\end{\${2:align*}} $3"
      ];
    };

    quarto_qquation_ref = {
      scope = "markdown, quarto";
      prefix = ["eqref"];
      body = ["([-@$1])"];
    };

    quarto_definition = {
      scope = "markdown, quarto";
      prefix = ["qdef"];
      body = [
        ":::{#def-$1}"
        "$2"
        ":::$3"
      ];
    };

    quarto_theorem = {
      scope = "markdown, quarto";
      prefix = ["qthm"];
      body = [
        ":::{#thm-$1}"
        "$2"
        ":::"
        "$3"
      ];
    };

    quarto_example = {
      scope = "markdown, quarto";
      prefix = ["qexm"];
      body = [
        ":::{#exm-$1}"
        "$2"
        ":::"
        "$3"
      ];
    };

    quarto_proof = {
      scope = "markdown, quarto";
      prefix = ["qproof"];
      body = [
        ":::{.proof}"
        "$1"
        ":::"
        "$2"
      ];
    };

    quarto_math_reference_block = {
      scope = "markdown, quarto";
      prefix = ["qlatex"];
      body = [
        ":::{#$1}"
        "$2"
        ":::"
        "$3"
      ];
    };
  };

  keybindings = [
    {
      key = "shift+escape";
      command = "workbench.action.terminal.toggleTerminal";
    }
    {
      key = "ctrl+alt+f";
      command = "workbench.action.quickTextSearch";
    }
    # {
    #   key = "shift+alt+c";
    #   command = "ltex.checkCurrentDocument";
    # }
  ];
}
