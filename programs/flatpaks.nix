{...}: [
  "flathub:app/me.iepure.devtoolbox//stable"
  "flathub:app/org.nickvision.tubeconverter//stable"
  "flathub:app/io.github.Soundux//stable"
  "flathub:app/com.ranfdev.Notify//stable"
  "flathub:app/org.gnome.World.Citations//stable"
  "flathub:app/com.github.rogercrocker.badabib//stable"
  "flathub:app/com.cassidyjames.butler//stable"
  "flathub:app/io.github.zen_browser.zen//stable"
]
