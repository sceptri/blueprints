{
  lib,
  config,
  ...
}: 
let
	port = 8096;
	host = "192.168.0.2";
	url = "jellyfin.zapadlo.name";

	unstablePkgs = import <nixos-unstable> {};
	pkg = unstablePkgs.jellyfin;
in {
	# The Thinkpad T440s' GPU supports H264, MPEG, JPEG, VC1
	# See: https://www.reddit.com/r/linuxmint/comments/132au0j/trying_to_activate_hardware_acceleration_on_my/
	# - https://github.com/jellyfin/jellyfin/issues/8740
	# - https://www.reddit.com/r/jellyfin/comments/11mo84v/question_on_hw_transcoding/
	# - https://github.com/intel/cartwheel-ffmpeg/issues/233
	services.jellyfin = {
		enable = true;
		openFirewall = true;
		user = "sceptrix";
		package = pkg;
	};

	services.nginx.virtualHosts = {
    "${url}" = {
      enableACME = true;
      forceSSL = true;

      extraConfig = ''
        proxy_set_header Host              $host;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host  $host;
      '';

      locations."/".proxyPass = "http://${host}:${toString port}";
    };
  };
}
