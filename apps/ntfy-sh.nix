{
  lib,
  config,
  ...
}: let
  port = 9090;
  host = "ntfy.zapadlo.name";
in {
  services.ntfy-sh = {
    enable = false; # See: https://github.com/NixOS/nixpkgs/issues/286480
    settings = {
      listen-http = ":${toString port}";
      base-url = "https://${host}";
    };
  };

  services.nginx.virtualHosts = {
    "${host}" = {
      forceSSL = true;
      enableACME = true;

      locations."/".proxyPass = "http://localhost:${toString port}";
    };
  };
}
