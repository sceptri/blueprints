{
  lib,
  config,
  ...
}: let
  unstablePkgs = import <nixos-unstable> {};

  rootDir = ../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;

  vikunjaPort = 3456;
in {
  services.vikunja = {
    enable = true;

    package = unstablePkgs.vikunja;

    frontendScheme = "https";
    frontendHostname = "vikunja.zapadlo.name";
    port = vikunjaPort;

    settings = {
      mailer = {
        enabled = true;

        host = "smtp.seznam.cz";
        port = 25;

        username = "vikunja@zapadlo.name";
        password = secrets.services.vikunja;

        fromemail = "vikunja@zapadlo.name";
      };
    };
  };

  services.nginx.virtualHosts = {
    "vikunja.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      extraConfig = ''
        proxy_set_header Host              $host;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host  $host;
      '';

      locations."/".proxyPass = "http://localhost:${toString vikunjaPort}";
    };
  };
}
