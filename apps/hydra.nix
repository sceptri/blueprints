{
  lib,
  config,
  ...
}: {
  # Hydra does not seem to fit my usecase as a workflow automation/Jenkins replacement right now
  # Also it keeps wasting space in /var/lib/hydra for whatever reason
  services.hydra = {
    enable = false;
    hydraURL = "localhost"; # externally visible URL
    port = 3031;
    notificationSender = "hydra@localhost"; # e-mail of hydra service

    # a standalone hydra will require you to unset the buildMachinesFiles list to avoid using a nonexistent /etc/nix/machines
    buildMachinesFiles = [];
    # you will probably also want, otherwise everything will be built from scratch
    useSubstitutes = true;
  };
}
