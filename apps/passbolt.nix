# INFO: For inspiration, see https://github.com/DestinyofYeet/nix-config/blob/05d043dfcc88b1ce496c0e061687795f73876add/server/nix-server/services/passbolt.nix#L12
{
  lib,
  config,
  pkgs,
  ...
}: let
  port = 8044;
  host = "192.168.0.2";
  url = "passbolt.zapadlo.name";

  unstablePkgs = import <nixos-unstable> {};

  rootDir = ../.;
  args = {};

	pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
	secrets = importConfig "secrets" "general" args;
in {
  systemd.tmpfiles.rules = [
    "d  /var/storage/passbolt          0777 sceptrix - - -"
    "d  /var/storage/passbolt/gpg      0777 sceptrix - - -"
    "d  /var/storage/passbolt/jwt      0777 sceptrix - - -"
    "d  /var/storage/passbolt/mariadb  0777 sceptrix - - -" 
  ];
    
  virtualisation.oci-containers.containers = {
    passbolt = {
      image = "passbolt/passbolt:latest-ce";
      dependsOn = ["pb-mariadb"];

      autoStart = true;
      serviceName = "passbolt";

      environment = {
        DATASOURCES_DEFAULT_HOST = "127.0.0.1";
        DATASOURCES_DEFAULT_PASSWORD = secrets.passbolt.mariadb.user-password;
        DATASOURCES_DEFAULT_USERNAME = "passbolt";
        DATASOURCES_DEFAULT_DATABASE = "passbolt";

        EMAIL_DEFAULT_FROM_NAME = "Passbolt";
        EMAIL_DEFAULT_FROM = "passbolt@zapadlo.name";
        EMAIL_TRANSPORT_DEFAULT_HOST = "smtp.seznam.cz";
        EMAIL_TRANSPORT_DEFAULT_PORT = "465";
        EMAIL_TRANSPORT_DEFAULT_USERNAME = "passbolt@zapadlo.name";
        EMAIL_TRANSPORT_DEFAULT_PASSWORD = secrets.passbolt.mail;
        EMAIL_TRANSPORT_DEFAULT_TLS = "true";

        APP_FULL_BASE_URL = "https://${url}";
        PASSBOLT_KEY_EMAIL = "stepan@zapadlo.name";

        PASSBOLT_SSL_FORCE = "true";
      };

      # TODO: Ensure volume dirs by temp-files
      volumes = [
        # WARN: /var/storage/passbolt needed to be created
        "/var/storage/passbolt/gpg:/etc/passbolt/gpg"
        "/var/storage/passbolt/jwt:/etc/passbolt/jwt"
      ];

      extraOptions = [
        "--network=container:pb-mariadb"
      ];
    };

    pb-mariadb = {
      image = "mariadb";
      autoStart = true;

      volumes = [
        "/var/storage/passbolt/mariadb:/var/lib/mysql"
      ];

      # The passbolt ports have to be HERE, as the passbolt container inherits this network
      ports = ["${toString port}:80" "${toString (port + 1)}:443"]; #server locahost : docker localhost
      
      environment = {
        MARIADB_USER = "passbolt";
        MARIADB_PASSWORD = secrets.passbolt.mariadb.user-password;
        MARIADB_DATABASE = "passbolt";
        MARIADB_ROOT_PASSWORD = secrets.passbolt.mariadb.root-password;
      };
    };
  };

  # Needed to run:
  # sudo docker exec passbolt su -m -c "bin/cake passbolt register_user -u stepan@zapadlo.name -f Stepan -l Zapadlo -r admin" -s /bin/sh www-data

  services.nginx.virtualHosts = {
    "${url}" = {
      enableACME = true;
      forceSSL = true;

      extraConfig = ''
        proxy_set_header Host              $host;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host  $host;
      '';

      locations."/".proxyPass = "https://${host}:${toString (port + 1)}";
    };
  };
}
