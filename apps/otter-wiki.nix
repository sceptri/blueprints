{
  lib,
  config,
  ...
}: let
  port = 7689;

  rootDir = ../.;
  args = {};

	pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
	secrets = importConfig "secrets" "general" args;
in {
  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
    };

    oci-containers = {
      backend = "podman";
      containers.otterwiki = {
        image = "redimp/otterwiki:2.8";
        autoStart = true;

        environment = {
          SITE_NAME = "Sceptri's Wiki";
          SITE_DESCRIPTION = "A simple wiki for when Memos isn't enough and Quarto is too much!";

					MAIL_DEFAULT_SENDER = "wiki@zapadlo.name";
					MAIL_SERVER = "smtp.seznam.cz";
					MAIL_USERNAME = "wiki@zapadlo.name";
					MAIL_PASSWORD = secrets.mail.wiki;

					WRITE_ACCESS = "REGISTERED";
					AUTO_APPROVAL = "False";
					NOTIFY_ADMINS_ON_REGISTER = "True";
					EMAIL_NEEDS_CONFIRMATION = "True";
					# May add more here, see https://otterwiki.com/Configuration
        };

        volumes = [
          "/var/storage/otterwiki:/app-data" # WARN: /var/storage/otterwiki needed to be created
        ];

        ports = ["${toString port}:80"]; #server locahost : docker localhost
      };
    };
  };

  services.nginx.virtualHosts = {
    "wiki.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      extraConfig = ''
        proxy_set_header Host              $host;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host  $host;
      '';

      locations."/".proxyPass = "http://localhost:${toString port}";
    };
  };
}
