{
  lib,
  config,
  ...
}: let
  keycloakHost = "keycloak.zapadlo.name";
  sslDir = "/var/lib/acme/${keycloakHost}/";

  rootDir = ../.;
  args = {};

	pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;
in {
  services.keycloak = {
    enable = true;
    initialAdminPassword = secrets.services.keycloak;
    database = {
      type = "postgresql";
      passwordFile = "/var/passwords/keycloak";
    };
    settings = {
      hostname = keycloakHost;
      hostname-admin-url = "https://" + keycloakHost + "/";
      http-enabled = true;
      http-port = 7787;
      https-port = 7789;
      http-host = "127.0.0.1";

      proxy = "passthrough";
    };
    sslCertificate = sslDir + "cert.pem";
    sslCertificateKey = sslDir + "key.pem";
  };

  services.nginx.virtualHosts = {
    "${keycloakHost}" = {
      listen = [
        {
          addr = "0.0.0.0";
          port = 7788;
        }
      ];
      forceSSL = true;
      enableACME = true;
      root = "/var/www/${keycloakHost}";
      locations."/".proxyPass = "https://localhost:7789";
    };
  };

  security.acme.acceptTerms = true;
  security.acme.defaults.email = "stepan@zapadlo.name";
}
