{
  lib,
  pkgs,
  config,
  ...
}: let
  rootDir = ../.;
  args = {inherit config lib pkgs;};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));

  secrets = importConfig "secrets" "general" args;

  port = 3333;
  host = "192.168.0.2";
  url = "flood.zapadlo.name";

  transmissionPort = 9091;
  transmissionUrl = "transmission.zapadlo.name";
in {
  imports = [(pathTo "modules" "flood")];

  services.Flood = {
    enable = true;

    webPassword = secrets.services.flood;

    backends.transmission.enable = true;
    backends.transmission.settings.settings.incomplete-dir-enabled = false;
    backends.transmission.settings.settings.rpc-port = transmissionPort;
    backends.transmission.settings.settings.download-dir = "${config.services.transmission.home}/Stream";

    mainDir = "/drives/Jupyter";
  };

  # INFO: It was necessary to copy over .config and create a Downloads directory in the Mars storage
  # TODO: Sort this out...

  services.nginx.virtualHosts = {
    "${url}" = {
      forceSSL = true;
      enableACME = true;

      extraConfig = ''
        error_page 401 = @error401;
      '';

      locations = {
        "/" = {
          extraConfig = ''
            auth_request /sso-auth;
            auth_request_set $cookie $upstream_http_set_cookie;
            add_header Set-Cookie $cookie;
          '';

          proxyPass = "http://${host}:${toString port}";
        };

        "/logout" = {
          return = "302 https://sso.zapadlo.name/logout?go=$scheme://$http_host/";
        };

        "/sso-auth" = {
          extraConfig = ''
            internal;

            proxy_pass_request_body off;
            proxy_set_header Content-Length "";

            proxy_set_header X-Origin-URI $request_uri;
            proxy_set_header X-Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Application "Flood";

          '';

          proxyPass = "https://sso.zapadlo.name/auth";
        };

        "@error401" = {
          return = "302 https://sso.zapadlo.name/login?go=$scheme://$http_host$request_uri";
        };
      };
    };

    "${transmissionUrl}" = {
      forceSSL = true;
      enableACME = true;

      extraConfig = ''
        error_page 401 = @error401;
      '';

      locations = {
        "/" = {
          extraConfig = ''
            auth_request /sso-auth;
            auth_request_set $cookie $upstream_http_set_cookie;
            add_header Set-Cookie $cookie;
          '';

          proxyPass = "http://${host}:${toString transmissionPort}";
        };

        "/logout" = {
          return = "302 https://sso.zapadlo.name/logout?go=$scheme://$http_host/";
        };

        "/sso-auth" = {
          extraConfig = ''
            internal;

            proxy_pass_request_body off;
            proxy_set_header Content-Length "";

            proxy_set_header X-Origin-URI $request_uri;
            proxy_set_header X-Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Application "Transmission";

          '';

          proxyPass = "https://sso.zapadlo.name/auth";
        };

        "@error401" = {
          return = "302 https://sso.zapadlo.name/login?go=$scheme://$http_host$request_uri";
        };
      };
    };
  };
}
