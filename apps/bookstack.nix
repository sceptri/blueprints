{
  lib,
  config,
  ...
}: {
  services.bookstack = {
    enable = true;

    appKeyFile = "/var/keys/bookstack-appkey";
    database.createLocally = true;

    hostname = "bookstack.zapadlo.name";
    appURL = "https://bookstack.zapadlo.name";

    nginx.serverName = "bookstack.zapadlo.name";
    nginx.enableACME = true;
    nginx.forceSSL = true;

    config = {
      # Allow pasting iframes from memos
      ALLOWED_IFRAME_SOURCES = "https://memos.zapadlo.name";

      # Make session last 1 week (in minutes)
      SESSION_LIFETIME = 10080;
    };

    mail = {
      user = "bookstack@zapadlo.name";
      passwordFile = "/var/passwords/bookstack";

      host = "smtp.seznam.cz";
      port = 465;

      fromName = "BookStack";
      from = "bookstack@zapadlo.name";

      encryption = "tls";
    };
  };
}
