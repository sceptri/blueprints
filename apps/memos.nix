{
  lib,
  config,
  ...
}: {
  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
    };

    oci-containers = {
      backend = "podman";
      containers.memos = {
        image = "neosmemo/memos:0.24.0";
        autoStart = true;

        volumes = [
          "/var/storage/memos:/var/opt/memos"
        ];

        ports = ["8071:5230"]; #server locahost : docker localhost
      };
    };
  };

  services.nginx.virtualHosts = {
    "memos.zapadlo.name" = {
      enableACME = true;
      forceSSL = true;

      extraConfig = ''
        proxy_set_header Host              $host;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host  $host;
      '';

      locations."/".proxyPass = "http://localhost:8071";
    };
  };
}
