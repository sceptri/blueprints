{
  lib,
  config,
  ...
}: {
  services.transmission = {
    enable = true;
    openRPCPort = true; #Open firewall for RPC
    settings = {
      # Override default settings
      # IP settings
      rpc-bind-address = "0.0.0.0"; #Bind to own IP
      rpc-whitelist = "127.0.0.1,192.168.0.15"; #Whitelist your remote machine (10.0.0.1 in this example)
    };
  };
}
