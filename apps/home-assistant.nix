{
  lib,
  pkgs,
  config,
  ...
}: let
  modPath = ../modules;
in {
  imports = [(modPath + "/esphome.nix")];

  services.home-assistant = {
    enable = true;
    openFirewall = true;

    extraComponents = [
      "default_config"
      "esphome"
      "met"
      "ipp"
      "mqtt"
      "tomorrowio"
      "stream"
      "camera"
      "shopping_list"
			"tractive"
      "systemmonitor"
			"isal"
    ];

    extraPackages = python3Packages:
      with python3Packages;
        [
          # postgresql support
          psycopg2
          aiohomekit
          aiodiscover
          pyipp
          securetar
          scapy
          aioesphomeapi
          aiohttp-cors
          bleak-retry-connector
          bleak
          bluetooth-adapters
          bluetooth-auto-recovery
          bluetooth-data-tools
          dbus-fast
          esphome-dashboard-api
          fnvhash
          ifaddr
          pyserial
          pyudev
          sqlalchemy
          zeroconf
          janus
          paho-mqtt
          pyipp
          pynacl
          pyturbojpeg
          async-upnp-client
          # async-interrupt
          hass-nabucasa
          fnv-hash-fast
          hassil
          home-assistant-frontend
          home-assistant-intents
          pillow
          psutil-home-assistant
          pytomorrowio
          mutagen
          webrtcvad
          tornado
					ical # Needed for Shopping list
					aiotractive # Probably needed for tractive
        ]
        ++ [pkgs.esptool];

    configWritable = true;
    config = {
      default_config = "";

      homeassistant = {
        unit_system = "metric";
        time_zone = "Europe/Prague";
        temperature_unit = "C";
        name = "Home";

        latitude = 49.2282467361167;
        longitude = 16.589671074963544;
        elevation = 237;

        # Default radius is way too big
        customize = {
          "zone.home" = {
            radius = 10;
          };
        };

        media_dirs = {
          complete = "/drives/Jupyter/Stream";
        };

        country = "CZ";

        external_url = "https://brno.zapadlo.name";
        internal_url = "http://192.168.0.2:8123";
      };

			# INFO: System monitor config was deprecated in YAML
      # sensor = [{
      #   platform = "systemmonitor";
      #   resources = [
      #     # Drives
      #     { type = "disk_use_percent"; arg = "/drives/Pluto"; }
      #     { type = "disk_use_percent"; arg = "/drives/Mars"; }
			# 		{ type = "disk_use_percent"; arg = "/drives/Jupyter"; }
      #     { type = "disk_use_percent"; arg = "/"; }

      #     # RAM & SWAP
      #     { type = "memory_use_percent"; }
      #     { type = "swap_use_percent"; }

      #     # CPU
      #     { type = "processor_use"; }
      #     { type = "processor_temperature"; }

      #     # Load
      #     { type = "load_1m"; }
      #     { type = "load_5m"; }
      #     { type = "load_15m"; }
      #   ];
      # }];

      http = {
        use_x_forwarded_for = true;
        trusted_proxies = [
          "192.168.1.10"
          "127.0.0.1"
          "::1"
        ];
      };

      mqtt = {
        switch = [
          {
            unique_id = "OpenBK7231N_55C68A40_relay_0";
            name = "Smart Socket";
            state_topic = "plug/0/get";
            command_topic = "plug/0/set";
            qos = 1;
            payload_on = 1;
            payload_off = 0;
            retain = true;
            availability = [
              {
                topic = "plug/connected";
              }
            ];
          }
        ];
        sensor = [
          {
            name = "Plug power";
            state_topic = "plug/power/get";
            unique_id = "plug_power";
            unit_of_measurement = "W";
            device_class = "power";
          }
          {
            name = "Plug Voltage";
            state_topic = "plug/voltage/get";
            unique_id = "plug_voltage";
            unit_of_measurement = "V";
            device_class = "voltage";
          }
          {
            name = "Plug Current";
            state_topic = "plug/current/get";
            unique_id = "plug_current";
            unit_of_measurement = "A";
            device_class = "current";
          }
          {
            name = "Energy Counter";
            state_topic = "plug/energycounter/get";
            unique_id = "plug_energy";
            unit_of_measurement = "kWh";
            device_class = "energy";
          }
          {
            name = "Energy Counter last hour";
            state_topic = "plug/energycounter_last_hour/get";
            unique_id = "plug_energy_hour";
            unit_of_measurement = "kWh";
            device_class = "energy";
          }
        ];
      };

      # INFO: Home Assistant says Esphome does not support yaml config
      # esphome = "";

      frontend = {
        themes = "!include_dir_merge_named themes";
      };

      "scene manual" = [];
      "scene ui" = "!include scenes.yaml";

      "automation manual" = [];
      "automation ui" = "!include automations.yaml";

      "person manual" = [];
      "person ui" = "!include people.yaml";

      stream = {
        "ll_hls" = true;
      };

      camera = "";
    };

    lovelaceConfigWritable = true;
  };

  services.esphome_custom = {
    enable = true;
  };

  services.nginx.virtualHosts = {
    "brno.zapadlo.name" = {
      forceSSL = true;
      enableACME = true;

      extraConfig = ''
        proxy_set_header  Upgrade  $http_upgrade;
        proxy_set_header  Connection  "upgrade";
      '';

      locations."/".proxyPass = "http://127.0.0.1:8123";
    };
  };
}
