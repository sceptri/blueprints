{
  lib,
  config,
  ...
}: let
  port = 9182;
  host = "plik.zapadlo.name";
in {
  services.plikd = {
    enable = true;
    openFirewall = true;
		settings = {
			ListenPort = port;
			# Create user with (under nix-shell -p plikd, config can be found with systemctl):
			# sudo plikd --config ./CHANGE_ME_plikd.cfg user create --login root --name Admin --admin    
			FeatureAuthentication = "forced";
		};
  };

  services.nginx.virtualHosts = {
    "${host}" = {
      forceSSL = true;
      enableACME = true;

      locations."/".proxyPass = "http://localhost:${toString port}";
			locations."/".extraConfig = ''
				client_max_body_size 10G;
			'';
    };
  };
}
