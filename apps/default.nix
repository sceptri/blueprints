{
  lib,
  config,
  ...
}: {
  imports = [
    ./dagu.nix
    ./vikunja.nix
    ./bookstack.nix
    ./memos.nix
    ./hydra.nix
    ./kestra.nix
    ./home-assistant.nix
    ./ntfy-sh.nix
    ./flood.nix
    ./avahi.nix
		./jellyfin.nix
    #./transmission.nix
    #./keycloak.nix
    ./openproject.nix
    #./redmine.nix
		#./huly.nix
		./plik.nix
		./passbolt.nix
		./otter-wiki.nix
  ];
}
