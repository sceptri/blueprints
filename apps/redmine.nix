{
  lib,
  config,
  ...
}: let
  redmine_port = 8867;
  redmine_port_internal = 8868;
  redmine_host = "test.zapadlo.name";

  rootDir = ../.;
  args = {};

	pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;
in {
  services.redmine = {
    enable = true;

    port = redmine_port_internal;

    settings = {
      delivery_method = "smtp";
      smtp_settings = {
        address = "smtp.seznam.cz";
        port = 25;

        user_name = "redmine@zapadlo.name";
        password = secrets.service.redmine;
      };
    };

    database = {
      type = "postgresql";
    };
  };

  services.nginx.virtualHosts = {
    "${redmine_host}" = {
      listen = [
        {
          addr = "0.0.0.0";
          port = redmine_port;
        }
      ];
      locations."/".proxyPass = "http://localhost:${(toString redmine_port_internal)}";
    };
  };
}
