{
  lib,
  config,
  ...
}: let
	rootDir = ../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));

  secrets = importConfig "secrets" "general" args;

  openproject_port = 8867;
  openproject_host = "openproject.zapadlo.name";
  openproject_ssl = true;
in {
  virtualisation = {
    podman = {
      enable = true;
      dockerCompat = true;
    };

    oci-containers = {
      backend = "podman";
      containers.openproject = {
        image = "openproject/openproject:15.2";
        autoStart = true;

        environment = {
          OPENPROJECT_HOST__NAME = openproject_host;
          OPENPROJECT_SECRET_KEY_BASE = secrets.openproject.secret;
          OPENPROJECT_HTTPS = if openproject_ssl then "true" else "false";
        };

        volumes = [
          "/var/lib/openproject/pgdata:/var/openproject/pgdata"
          "/var/lib/openproject/assets:/var/openproject/assets"
        ];

        ports = [((toString openproject_port) + ":80")]; #server locahost : docker localhost
      };
    };
  };

	services.nginx.virtualHosts = {
    "${openproject_host}" = {
      enableACME = true;
      forceSSL = true;

      extraConfig = ''
        proxy_set_header Host              $host;
        proxy_set_header X-Real-IP         $remote_addr;
        proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host  $host;
      '';

      locations."/".proxyPass = "http://localhost:${toString openproject_port}";
    };
  };
}
