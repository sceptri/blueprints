{
  lib,
  config,
  ...
}: let
  pkgPath = ../packages;
  modPath = ../modules;
  #dagu = pkgs.callPackage (pkgPath + "/dagu.nix") {};

  rootDir = ../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));

  secrets = importConfig "secrets" "general" args;
in {
  imports = [(modPath + "/dagu.nix")];

  services.dagu = {
    enable = false;
    host = "10.0.1.2";
    settings = {
      isBasicAuth = true;
      basicAuthUsername = "sceptrix";
      basicAuthPassword = secrets.services.dagu;
    };
  };
}
