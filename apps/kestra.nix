# INFO: Kestra basic-auth is right now implemented more or less just so Kestra does not complain about insecure data
# TODO: Allow basic-auth login to Kestra even without SSO authentication (maybe via custom route or subdomain?)
{
  lib,
  pkgs,
  config,
  ...
}: let
  pkgPath = ../packages;
  modPath = ../modules;

  rootDir = ../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "kestra" args;

  conv = (import ../utils/base64.nix) {inherit lib;};

  host = "192.168.0.2";
  healthPort = 8081;
in {
  imports = [(modPath + "/kestra.nix") (modPath + "/garage.nix")];

  services.kestra = {
    enable = true;

    version = {
      major = 21;
      minor = 4;
      hash = "17ky6ajifkhcsaykxrlp85r07452si7ib9p46lfarx2s7hd7y6ha";
    };

    port = 4041;
    host = host;

    kestra.queue.type = "postgres";
    kestra.repository.type = "postgres";
    kestra.storage.type = "minio";

    kestra.tutorial-flows.enabled = false;

    kestra.server.basic-auth = {
      enabled = true;
      username = secrets.auth.username;
      password = secrets.auth.password;
    };

    postgres.password = secrets.postgres.password;

    minio.accessKey = secrets.minio.accessKey;
    minio.secretKey = secrets.minio.secretKey;

    secrets = lib.mkMerge [
      (secrets.webhooks)
      (secrets.other)
    ];

    settings = {
      kestra = {
        variables = {
          globals = {
            env = "dev";
          };
        };
      };
      logger.levels."io.kestra.core.runners" = "TRACE";
    };

    packages = [
      pkgs.systemd
      pkgs.julia # Override to use julia --project or something like that
    ];

    plugins = [
      {name = "io.kestra.storage:storage-minio";}
      {name = "io.kestra.plugin:plugin-notifications";}
      {name = "io.kestra.plugin:plugin-mqtt";}
      {name = "io.kestra.plugin:plugin-fs";}
      {
        name = "io.kestra.plugin:plugin-script-shell";
        minor = 2;
      }
      {
        name = "io.kestra.plugin:plugin-script-julia";
        minor = 2;
      }
      {name = "io.kestra.plugin:plugin-serdes";}
      {name = "io.kestra.plugin:plugin-git";}
    ];
  };

  # INFO: After update, bucket was removed...
  # See: https://garagehq.deuxfleurs.fr/quick_start/index.html
  # Needed to:
  # 1. login as su
  # 2. `garage status`
  # 3. `garage layout assign -z dc1 -c 1G <node_id>` (`<node_id>` is the first column from the previous step)
  # 4. `garage layout apply --version 1`
  # 5. `garage bucket create kestra-bucket`
  # 6. `garage key create kestra-app-key`
  # 7. Update secrets/kestra.nix with the given output
  # 8. Add permisions with `garage bucket allow --read --write kestra-bucket --key kestra-app-key`
  services.Garage = {
    enable = true;
    package = pkgs.garage;
    settings = {
      rpc_bind_addr = "[::]:3901";
      rpc_public_addr = "127.0.0.1:3901";
      rpc_secret = secrets.garage.secret;

      replication_factor = 1;

      data_dir = "/var/lib/Garage/data";
      metadata_dir = "/var/lib/Garage/meta";

      s3_api = {
        s3_region = "garage";
        api_bind_addr = "[::]:3900";
        root_domain = ".s3.garage.localhost";
      };

      s3_web = {
        bind_addr = "[::]:3902";
        root_domain = ".web.garage.localhost";
        index = "index.html";
      };

      k2v_api = {
        api_bind_addr = "[::]:3904";
      };

      admin = {
        api_bind_addr = "0.0.0.0:3903";
        admin_token = secrets.garage.admin_token;
      };
    };
  };

  services.nginx.virtualHosts = let
    kestraPort = config.services.kestra.port;
    kestraCfg = config.services.kestra;
  in {
    "kestra.zapadlo.name" = {
      forceSSL = true;
      enableACME = true;

      extraConfig = ''
        error_page 401 = @error401;
      '';

      locations = {
        "/api" = {
          proxyPass = "http://${host}:${toString kestraPort}";

          extraConfig = ''
            auth_request /api-auth;
            auth_request_set $cookie $upstream_http_set_cookie;
            add_header Set-Cookie $cookie;
            ${lib.optionalString (kestraCfg.kestra.server.basic-auth.enabled) ''
              auth_request_set $dynamic_auth "Basic ${toString (conv.toBase64 "${kestraCfg.kestra.server.basic-auth.username}:${kestraCfg.kestra.server.basic-auth.password}")}";
              proxy_set_header Authorization $dynamic_auth;
            ''}
          '';
        };

        "/" = {
          extraConfig = ''
            auth_request /sso-auth;
            auth_request_set $cookie $upstream_http_set_cookie;
            add_header Set-Cookie $cookie;
            ${lib.optionalString (kestraCfg.kestra.server.basic-auth.enabled) ''
              auth_request_set $dynamic_auth "Basic ${toString (conv.toBase64 "${kestraCfg.kestra.server.basic-auth.username}:${kestraCfg.kestra.server.basic-auth.password}")}";
              proxy_set_header Authorization $dynamic_auth;
            ''}

            # INFO: Needed for SSE (see https://kestra.io/docs/administrator-guide/deployment/reverse-proxy)
            proxy_buffering off;
            proxy_cache off;
          '';

          proxyPass = "http://${host}:${toString kestraPort}";
        };

        "/logout" = {
          return = "302 https://sso.zapadlo.name/logout?go=$scheme://$http_host/";
        };

        "/sso-auth" = {
          extraConfig = ''
            internal;

            proxy_pass_request_body off;
            proxy_set_header Content-Length "";

            proxy_set_header X-Origin-URI $request_uri;
            proxy_set_header X-Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Application "kestra";
            proxy_set_header X-Group "admins";


            # INFO: Needed for SSE (see https://kestra.io/docs/administrator-guide/deployment/reverse-proxy)
            proxy_buffering off;
            proxy_cache off;
          '';

          proxyPass = "https://sso.zapadlo.name/auth";
        };

        "/api-auth" = {
          extraConfig = ''
            internal;

            proxy_pass_request_body off;
            proxy_set_header Content-Length "";

            proxy_set_header X-Origin-URI $request_uri;
            proxy_set_header X-Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Application "api-kestra";
            proxy_set_header X-Group "api-admins";
          '';

          proxyPass = "https://sso.zapadlo.name/auth";
        };

        "@error401" = {
          return = "302 https://sso.zapadlo.name/login?go=$scheme://$http_host$request_uri";
        };
      };
    };

    "health-kestra.zapadlo.name" = {
      forceSSL = true;
      enableACME = true;

      extraConfig = ''
        error_page 401 = @error401;
      '';

      locations = {
        "/" = {
          extraConfig = ''
            auth_request /api-auth;
            auth_request_set $cookie $upstream_http_set_cookie;
            add_header Set-Cookie $cookie;
            ${lib.optionalString (kestraCfg.kestra.server.basic-auth.enabled) ''
              auth_request_set $dynamic_auth "Basic ${toString (conv.toBase64 "${kestraCfg.kestra.server.basic-auth.username}:${kestraCfg.kestra.server.basic-auth.password}")}";
              proxy_set_header Authorization $dynamic_auth;
            ''}
          '';

          # TODO: Fix this hardcoded port
          proxyPass = "http://${host}:${toString healthPort}";
        };

        "/api-auth" = {
          extraConfig = ''
            internal;

            proxy_pass_request_body off;
            proxy_set_header Content-Length "";

            proxy_set_header X-Origin-URI $request_uri;
            proxy_set_header X-Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Application "health-kestra";
            proxy_set_header X-Group "api-admins";
          '';

          proxyPass = "https://sso.zapadlo.name/auth";
        };

        "/logout" = {
          return = "302 https://sso.zapadlo.name/logout?go=$scheme://$http_host/";
        };

        "@error401" = {
          return = "302 https://sso.zapadlo.name/login?go=$scheme://$http_host$request_uri";
        };
      };
    };
  };
}
