{
  pkgs,
  lib,
  ...
}: let
  unstablePkgs = import <nixos-unstable> {};

  # INFO: See this https://hoverbear.org/blog/declarative-gnome-configuration-in-nixos/
  packages = with pkgs.gnomeExtensions; [
    appindicator
    # Crashes the entire Gnome shell...
    # unstablePkgs.gnomeExtensions.pano
    gsconnect
    # espresso # Does not seem to work
    caffeine
    night-theme-switcher
    tactile
    # task-widget
    user-themes
    vitals
    launch-new-instance
    ddterm
    cronomix
    light-style
  ];
in {
  inherit packages;

  # Based on this issue: https://github.com/nix-community/home-manager/issues/284
  dconf = {
    # Enable installed extensions
    "org/gnome/shell".enabled-extensions = map (extension: extension.extensionUuid) packages;
    "org/gnome/shell".disabled-extensions = [];
    "org/gnome/shell".favorite-apps = [
      "firefox.desktop"
      "org.gnome.Nautilus.desktop"
      "code.desktop"
      "org.gnome.Calendar.desktop"
      "org.gnome.Lollypop.desktop"
    ];

    "org/gnome/shell/app-switcher".current-workspace-only = true;
    "org/gnome/shell/window-switcher".current-workspace-only = true;

    "org/gnome/mutter".dynamic-workspaces = true;

    "org/gnome/desktop/sound".allow-volume-above-100-percent = true;

    "org/gnome/desktop/interface".show-battery-percentage = true;
    "org/gnome/desktop/interface".clock-show-seconds = true;
    "org/gnome/desktop/interface".clock-show-weekday = true;
    "org/gnome/desktop/interface".enable-hot-corners = false;

    "org/gnome/desktop/peripherals/touchpad".tap-to-click = true;
    "org/gnome/desktop/wm/preferences".button-layout = "appmenu:minimize,maximize,close";

    "org/gnome/desktop/datetime".automatic-timezone = true;

    "org/gnome/system/location".enabled = true;

    # Weather
    "org/gnome/shell/weather".automatic-location = true;
    "org/gnome/shell/weather".locations = with lib.gvariant; [
      (mkVariant (mkTuple [
        (mkUint32 2)
        (mkVariant (mkTuple [
          "Brno"
          "LKTB"
          true
          [(mkTuple [(mkDouble "0.857829327355213") (mkDouble "0.291469985083053")])]
          [(mkTuple [(mkDouble "0.85870199198121022") (mkDouble "0.29030642643062599")])]
        ]))
      ]))
    ];

    # -- Extensions ---
    # GS Connect
    "org/gnome/shell/extensions/gsconnect".show-indicators = true;

    # Vitals
    "org/gnome/shell/extensions/vitals".position-in-panel = 0;

    # Night Theme Switcher
    "org/gnome/shell/extensions/nightthemeswitcher/gtk-variants".enabled = true;
    "org/gnome/shell/extensions/nightthemeswitcher/gtk-variants".day = "adw-gtk3";
    "org/gnome/shell/extensions/nightthemeswitcher/gtk-variants".night = "adw-gtk3-dark";
    "org/gnome/shell/extensions/nightthemeswitcher/time".manual-schedule = false;

    # Spotify Tray
    "org/gnome/shell/extensions/sp-tray".position = 0;

    # -- Keybindings --
    # INFO: https://heywoodlh.io/nixos-gnome-settings-and-keyboard-shortcuts
    "org/gnome/settings-daemon/plugins/media-keys" = {
      custom-keybindings = [
        "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/"
        "/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/"
      ];
    };
    "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0" = {
      name = "launch terminal";
      command = "kgx";
      binding = "<Ctrl>Escape";
    };
    "org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1" = {
      name = "launch files";
      command = "nautilus";
      binding = "<Super>e";
    };

    "org/gnome/dekstop/wm/keybindings".switch-windows = ["<Alt>Tab"];
    "org/gnome/dekstop/wm/keybindings".switch-windows-backward = ["<Shift><Alt>Tab"];
  };
}
