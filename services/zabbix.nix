{
  lib,
  config,
  ...
}: {
  services.zabbixAgent = {
    enable = true;
    server = "192.168.1.10";
    listen.ip = "192.168.0.2";
  };
}
