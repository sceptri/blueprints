{
  lib,
  config,
  ...
}: let
  rootDir = ../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;

	ssoPort = 7788;
in {
  services.nginx.sso = {
    enable = true;

    configuration = {
      listen = {
        addr = "0.0.0.0";
        port = ssoPort;
      };

      login = {
        title = "Sceptri's Private Space";
        default_method = "simple";
        hide_mfa_field = true;
        names.simple = "Username / Password";
      };

      cookie = {
        domain = ".zapadlo.name";
        authentication_key = secrets.sso.cookie;
      };

      acl = {
        rule_sets = [
          {
            rules = [
              {
                field = "x-group";
                equals = "users";
              }
            ];
            allow = ["@users"];
          }
          {
            rules = [
              {
                field = "x-group";
                equals = "admins";
              }
            ];
            allow = ["@admins"];
          }
          {
            rules = [
              {
                field = "x-group";
                equals = "api-admins";
              }
            ];
            allow = ["@admins" "@api"];
          }
          {
            rules = [
              {
                field = "x-group";
                present = false;
              }
              {
                field = "x-host";
                regexp = ".*";
              }
            ];
            allow = ["@_authenticated"];
          }
        ];
      };

      providers = {
        simple = {
          enable_basic_auth = false;
          users = secrets.sso.users;
          groups.admins = ["admin"];
          groups.users = ["verchan" "admin"];
        };
        token = {
          # INFO: Requires header "Authorization: Token <token>"
          tokens = secrets.sso.tokens;
          groups.api = ["x-gitlab-token" "general-api"];
        };
      };
    };
  };

  services.nginx.virtualHosts = {
    "sso.zapadlo.name" = {
      forceSSL = true;
      enableACME = true;

      locations."/".proxyPass = "http://localhost:${toString ssoPort}";
    };
  };

  # INFO: Necessary for Kestra api security
	# TODO(#15): Allow for non-SSO authorizastion to Kestra via basic-auth
  services.nginx.appendHttpConfig = ''
    map $http_x_gitlab_token $auth_token {
      "~*(.+)" "Token $1";
      default $http_authorization;
    }
    proxy_set_header Authorization $auth_token;
  '';
}
