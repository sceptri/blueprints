{
  lib,
  config,
  ...
}: let
  rootDir = ../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;
in {
  services.mosquitto = {
    enable = true;

    listeners = [
      {
        users = {
          kestra = {
            # TODO: Change MQTT passwords (and how they are used in Kestra)
            password = secrets.mqtt.kestra;
            acl = [
              "readwrite misc/#"
              "readwrite fertilizer/#"
              "readwrite home/#"
            ];
          };

          fertilizer = {
            password = secrets.mqtt.fertilizer;
            acl = [
              "readwrite misc/#"
              "readwrite fertilizer/#"
              "readwrite home/#"
            ];
          };

          hass = {
            password = secrets.mqtt.hass;
            acl = [
              "readwrite misc/#"
              "readwrite plug/#"
              "readwrite fertilizer/#"
            ];
          };

          plug = {
            password = secrets.mqtt.plug;
            acl = [
              "readwrite plug/#"
            ];
          };
        };
      }
    ];
  };
}
