{
  lib,
  config,
  ...
}: {
  imports = [
    ./zabbix.nix
    #./wireguard.nix
    ./mosquitto.nix
    ./nginx-sso.nix
    ./update-containers.nix
  ];
}
