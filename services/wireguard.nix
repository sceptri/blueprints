{
  lib,
  config,
  ...
}: {
  networking.wireguard.enable = true;
  networking.wireguard.interfaces = {
    wg0 = {
      ips = ["10.0.1.2/32"];
      listenPort = 13231;

      privateKeyFile = "/root/wireguard-keys/private";

      # Vodafone DNS is not visible outside of Vodafone network, so we need to route requests to it via the wifi
      # postSetup = ["ip route add 95.85.216.81/32 via 192.168.0.1" "ip route add 31.30.90.11/32 via 192.168.0.1"];
      # postShutdown = ["ip route del 95.85.216.81" "ip route del 31.30.90.11"];

      peers = [
        {
          publicKey = "0GHIjDVfEMWIWbyIJo90mdcmEA9JzO9Sbh44Vz4SaGw=";
          allowedIPs = ["0.0.0.0/0"];
          endpoint = "zapadlo.name:13231";
          persistentKeepalive = 25;
        }
      ];
    };
  };
}
