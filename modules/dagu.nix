{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  pkgPath = ../packages;
  cfg = config.services.dagu;
  format = pkgs.formats.yaml {};
  configFile = format.generate "dagu-config.yaml" (cfg.settings
    // {
      port = cfg.port;
      host = cfg.host;
      dags = cfg.dags;
    });
  daguPackage = pkgs.callPackage (pkgPath + "/dagu.nix") {};
in {
  options.services.dagu = with lib; {
    enable = mkEnableOption "dagu service";

    package = mkOption {
      default = daguPackage;
      type = types.package;
      defaultText = "pkgs.callPackage (${pkgPath + "/dagu.nix"}) {};";
    };

    settings = mkOption {
      type = format.type;
      default = {};
    };

    user = mkOption {
      type = types.str;
      default = "dagu";
    };

    group = mkOption {
      type = types.str;
      default = "dagu";
    };

    host = mkOption {
      type = types.str;
      default = "localhost";
    };

    port = mkOption {
      type = types.port;
      default = 4040;
    };

    directory = mkOption {
      type = types.path;
      default = "/var/storage/dagu";
    };

    dags = mkOption {
      type = types.path;
      default = "/var/storage/dagu/dags";
    };
  };

  config = lib.mkIf cfg.enable {
    users = {
      groups = mkIf (cfg.group == "dagu") {
        dagu = {};
      };
      users = mkIf (cfg.user == "dagu") {
        dagu = {
          group = cfg.group;
          isSystemUser = true;
        };
        "${config.services.nginx.user}".extraGroups = [cfg.group];
      };
    };

    systemd.services.dagu = {
      description = "dagu server";

      after = ["network.target"];
      wantedBy = ["multi-user.target"];

      path = ["/run/current-system/sw/bin" cfg.package] ++ config.environment.systemPackages ++ config.environment.defaultPackages;
      restartTriggers = [configFile];

      script = ''
        # run the server
        ${cfg.package}/bin/dagu server --port=${toString cfg.port} --host=${toString cfg.host} --config=${configFile}
      '';
      environment = {
        DAGU_HOME = cfg.directory;
        NIX_PATH = lib.concatStringsSep ":" config.nix.nixPath;
      };

      serviceConfig = {
        Type = "simple";
        User = cfg.user;
        WorkingDirectory = cfg.directory;
        RuntimeDirectory = "dagu/cache";
        RuntimeDirectoryMode = "0700";
        Restart = "always";
      };
    };

    environment.etc."dagu/keepalive.sh" = {
      mode = "0770";
      text = ''
        #!/bin/bash

        process="${cfg.package}/bin/dagu scheduler"
        command="${cfg.package}/bin/dagu scheduler --config=${configFile}"

        if ps ax | grep -v grep | grep "$process" > /dev/null
        then
          exit
        else
          $command &
        fi

        exit
      '';
    };

    services.cron.systemCronJobs = [
      "*/1 * * * *  root /etc/dagu/keepalive.sh"
    ];

    systemd.tmpfiles.rules = [
      "d  ${cfg.directory}       0710 ${cfg.user} ${cfg.group} - -"
      "d  ${cfg.directory}/dags  0700 ${cfg.user} ${cfg.group} - -"
      "d  ${cfg.directory}/data  0700 ${cfg.user} ${cfg.group} - -"
      "d  ${cfg.directory}/logs  0700 ${cfg.user} ${cfg.group} - -"
    ];
  };
}
