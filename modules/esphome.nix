{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  cfg = config.services.esphome_custom;
in {
  options.services.esphome_custom = with lib; {
    enable = mkEnableOption "esphome service";

    directory = mkOption {
      type = types.path;
      default = config.services.home-assistant.configDir + "/esphome/";
    };

    package = mkOption {
      default = pkgs.esphome;
      type = types.package;
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.services.esphome_custom = {
      description = "ESPHome";
      after = ["network.target"];
      wantedBy = ["multi-user.target"];
      serviceConfig = {
        User = "hass";
        Group = "hass";
        Restart = "on-failure";
        WorkingDirectory = cfg.directory;
        ExecStart = "${cfg.package}/bin/esphome dashboard ${cfg.directory}";
      };
    };

    systemd.tmpfiles.rules = [
      "d  ${cfg.directory}       0777 hass hass - -"
    ];
  };
}
