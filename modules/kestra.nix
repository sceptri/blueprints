{
  config,
  lib,
  pkgs,
  ...
}:
with lib; let
  pkgPath = ../packages;
  cfg = config.services.kestra;

  format = pkgs.formats.yaml {};
  configFile = format.generate "kestra-config.yaml" (cfg.settings
    // {
      inherit (cfg) kestra;
      inherit (cfg) micronaut;
      inherit (cfg) datasources;
      inherit (cfg) endpoints;
    });

  kestraPackage = version: (pkgs.callPackage (pkgPath + "/kestra.nix") {inherit version;});

  conv = (import ../utils/base64.nix) {inherit lib;};

  endpointOptions = {name, ...}:
    with lib; {
      options = {
        "basic-auth" = {
          username = mkOption {
            type = types.str;
            default = "admin";
          };

          password = mkOption {
            type = types.str;
            default = "admin";
          };
        };
      };
    };
in {
  options.services.kestra = with lib; {
    enable = mkEnableOption "Kestra service";

    version = {
      breaking = mkOption {
        type = types.int;
        default = 0;
      };

      major = mkOption {
        type = types.int;
      };

      minor = mkOption {
        type = types.int;
        default = 0;
      };

      hash = mkOption {
        type = types.str;
        default = "";
      };
    };

    package = mkOption {
      default = kestraPackage cfg.version;
      type = types.package;
      defaultText = "pkgs.callPackage (${pkgPath + "/kestra.nix"}) {};";
    };

    user = mkOption {
      type = types.str;
      default = "kestra";
    };

    group = mkOption {
      type = types.str;
      default = "kestra";
    };

    settings = mkOption {
      type = format.type;
      default = {};
    };

    host = mkOption {
      type = types.str;
      default = "localhost";
    };

    port = mkOption {
      type = types.port;
      default = 4043;
    };

    frontendScheme = mkOption {
      type = types.str;
      default = "http";
    };

    directory = mkOption {
      type = types.path;
      default = "/var/storage/kestra";
    };

    pluginDirectory = mkOption {
      type = types.path;
      default = cfg.directory + "/plugins";
    };

    tmpDirectory = mkOption {
      type = types.path;
      default = "/tmp/kestra-wd/tmp";
    };

    secrets = mkOption {
      type = types.attrsOf types.str;
      default = {};
    };

    minio = {
      endpoint = mkOption {
        type = types.str;
        default = "192.168.0.2";
      };

      port = mkOption {
        type = types.port;
        default = 3900;
      };

      accessKey = mkOption {
        type = types.str;
        default = "";
      };

      secretKey = mkOption {
        type = types.str;
        default = "";
      };

      region = mkOption {
        type = types.str;
        default = "garage";
      };

      secure = mkOption {
        type = types.bool;
        default = false;
      };

      bucket = mkOption {
        type = types.str;
        default = "kestra-bucket";
      };
    };

    postgres = {
      host = mkOption {
        type = types.str;
        default = cfg.host;
      };

      port = mkOption {
        type = types.port;
        default = 5432;
      };

      dbName = mkOption {
        type = types.str;
        default = "kestra";
      };

      username = mkOption {
        type = types.str;
        default = "kestra";
      };

      password = mkOption {
        type = types.str;
        default = "admin";
      };
    };

    endpoints = mkOption {
      type = types.attrsOf (types.submodule endpointOptions);
      default = {};
      example = literalExpression ''
        {
          all = {
            basic-auth = {
              username = "admin";
              password = "admin";
            };
          };
        }
      '';
      description = "configuration of authentication for endpoints";
    };

    packages = mkOption {
      type = types.listOf types.package;
      default = [];
    };

    plugins = mkOption {
      type = types.listOf types.attrs;
      default = [];
    };

    kestra = {
      url = mkOption {
        type = types.str;
        default = cfg.frontendScheme + "://" + cfg.host + ":" + (toString cfg.port) + "/";
      };

      plugins = mkOption {
        type = format.type;
        default = {
          central = {
            url = "https://repo.maven.apache.org/maven2/";
          };

          jcenter = {
            url = "https://jcenter.bintray.com/";
          };

          kestra = {
            url = "https://dl.bintray.com/kestra/maven";
          };
        };
      };

      tasks = {
        tmp-dir = {
          path = mkOption {
            type = types.path;
            default = cfg.tmpDirectory;
          };
        };
      };

      repository = {
        type = mkOption {
          type = types.str;
          default = "postgres";
        };
      };

      queue = {
        type = mkOption {
          type = types.str;
          default = "postgres";
        };
      };

      storage = {
        type = mkOption {
          type = types.str;
          default = "local";
        };

        local = {
          base-path = mkOption {
            type = types.path;
            default = cfg.directory;
          };
        };

        minio = {
          endpoint = mkOption {
            type = types.str;
            default = cfg.minio.endpoint;
          };

          port = mkOption {
            type = types.port;
            default = cfg.minio.port;
          };

          accessKey = mkOption {
            type = types.str;
            default = cfg.minio.accessKey;
          };

          secretKey = mkOption {
            type = types.str;
            default = cfg.minio.secretKey;
          };

          region = mkOption {
            type = types.str;
            default = cfg.minio.region;
          };

          secure = mkOption {
            type = types.bool;
            default = cfg.minio.secure;
          };

          bucket = mkOption {
            type = types.str;
            default = cfg.minio.bucket;
          };
        };
      };

      server = {
        basic-auth = {
          enabled = mkEnableOption "Basic authentication";

          username = mkOption {
            type = types.str;
            default = "admin";
          };

          # INFO: Default values per docs: https://kestra.io/docs/configuration-guide/server
          password = mkOption {
            type = types.str;
            default = "kestra";
          };
        };
      };

      tutorial-flows = {
        enabled = mkEnableOption "Tutorial flows";
      };
    };

    micronaut = {
      server = {
        port = mkOption {
          type = types.port;
          default = cfg.port;
        };
      };
    };

    datasources = {
      postgres = {
        username = mkOption {
          type = types.str;
          default = cfg.postgres.username;
        };
        password = mkOption {
          type = types.str;
          default = cfg.postgres.password;
        };

        driverClassName = mkOption {
          type = types.str;
          default = "org.postgresql.Driver";
        };

        url = mkOption {
          type = types.str;
          default =
            "jdbc:postgresql://"
            + cfg.postgres.host
            + ":"
            + (toString cfg.postgres.port)
            + "/"
            + cfg.postgres.dbName;
        };
      };
    };
  };

  config = lib.mkIf cfg.enable {
    users = {
      groups = mkIf (cfg.group == "kestra") {
        kestra = {};
      };
      users = mkIf (cfg.user == "kestra") {
        kestra = {
          group = cfg.group;
          isSystemUser = true;
          extraGroups = ["wheel"];
          home = "/home/kestra";
          createHome = true;
          homeMode = "777";
        };
        "${config.services.nginx.user}".extraGroups = [cfg.group];
      };
    };

    systemd.services.kestra-init = {
      description = "kestra init";

      after = ["network.target" "postgresql.service"];
      requires = ["postgresql.service"];

      script = ''
            if ! [ -e ${cfg.directory}/misc/.db-created ]; then
              ${pkgs.sudo}/bin/sudo -u ${config.services.postgresql.superUser} \
                ${config.services.postgresql.package}/bin/psql -U ${config.services.postgresql.superUser} \
                -c "CREATE USER ${cfg.postgres.username} WITH ENCRYPTED PASSWORD '${cfg.postgres.password}'"
              ${pkgs.sudo}/bin/sudo -u ${config.services.postgresql.superUser} \
                ${config.services.postgresql.package}/bin/psql -U ${config.services.postgresql.superUser} \
                -c "CREATE DATABASE ${cfg.postgres.dbName} OWNER ${cfg.postgres.username} ENCODING 'UTF8' TEMPLATE template0"
              touch ${cfg.directory}/misc/.db-created
            fi

            mkdir -p ${cfg.tmpDirectory}
            chmod -R a+rw ${cfg.tmpDirectory}
            chmod -R a+rw /var/tmp

            # Remove old plugins
            test -n "$(ls -A ${cfg.pluginDirectory})" && rm ${cfg.pluginDirectory}/*

            ${concatStringsSep "\n" (forEach (cfg.plugins) (
          plugin: "${cfg.package}/bin/kestra plugins install ${plugin.name}:${toString cfg.version.breaking}.${toString cfg.version.major}.${
            if builtins.hasAttr "minor" plugin
            then (toString plugin.minor)
            else "0"
          } --config=${configFile} --plugins=${cfg.pluginDirectory}"
        ))}

        ${cfg.package}/bin/kestra sys state-store migrate --config=${configFile} --plugins=${cfg.pluginDirectory}
      '';

      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        PermissionsStartOnly = true;
      };
    };

    systemd.services.kestra = {
      description = "kestra server";

      after = ["kestra-init.service" "network.target" "postgresql.service"];
      requires = ["kestra-init.service"];
      wantedBy = ["multi-user.target"];

      # [HL-82] Removed config.environment.defaultPackages because of openssl-1.1.1 version
      path = ["/run/current-system/sw/bin" cfg.package] ++ config.environment.systemPackages ++ cfg.packages;
      restartTriggers = [configFile];
      restartIfChanged = true;

      script = ''
        # run the server
        ${cfg.package}/bin/kestra server standalone --config=${configFile} --plugins=${cfg.pluginDirectory}
      '';

      environment =
        {
          NIX_PATH = lib.concatStringsSep ":" config.nix.nixPath;
          NIXOS_CONFIGURATION_DIR = config.environment.sessionVariables.NIXOS_CONFIGURATION_DIR + "/.";
        }
        // mapAttrs' (name: value: nameValuePair ("SECRET_" + name) (conv.toBase64 value)) cfg.secrets;

      serviceConfig = {
        Type = "simple";
        PermissionsStartOnly = true;
        User = cfg.user;
        WorkingDirectory = cfg.directory;
        RuntimeDirectory = "kestra/cache";
        RuntimeDirectoryMode = "0700";
        Restart = "always";
      };
    };

    systemd.tmpfiles.rules = [
      "d  ${cfg.directory}       0710 ${cfg.user} ${cfg.group} - -"
      "d  ${cfg.pluginDirectory}   0777  ${cfg.user}  ${cfg.group}"
    ];

    services.postgresql = {
      enable = true;
      enableTCPIP = true;
      identMap = ''
        kestra-map kestra kestra
      '';
      authentication = ''
        local kestra all ident map=kestra-map
        ${optionalString (cfg.host != "localhost") ''
          host kestra all ${cfg.host}/32 md5
        ''}
      '';
    };

    security.polkit.extraConfig = ''
      polkit.addRule(function(action, subject) {
        if (action.id == "org.freedesktop.systemd1.manage-units" &&
            subject.user == "${cfg.user}") {
            return polkit.Result.YES;
        } });
    '';
  };
}
