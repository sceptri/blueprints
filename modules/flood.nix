{
  config,
  lib,
  pkgs,
  builtins,
  ...
}:
with lib; let
  cfg = config.services.Flood;
  torrentHost = "127.0.0.1";

  unstablePkgs = import <nixos-unstable> {};

  tranmissionDefaults = {
    openRPCPort = true;
    openPeerPorts = true;
    openFirewall = true;

    package = unstablePkgs.transmission_4;

    home = cfg.mainDir;

    user = cfg.user;
    group = cfg.group;

    downloadDirPermissions = "777";

    settings = {
      umask = 2;

      rpc-bind-address = "0.0.0.0";
      rpc-whitelist = "127.0.0.1,192.168.0.*,192.168.1.*";

      rpc-user = cfg.user;
      rpc-password = cfg.webPassword;
    };
  };

  rtorrentDefaults = {
    dataDir = cfg.mainDir;
    dataPermissions = "0777";
    user = cfg.user;
    group = cfg.group;
    openFirewall = true;
  };
in {
  options.services.Flood = with lib; {
    enable = mkEnableOption "Flood torrent WebUI NixOS service";

    package = mkOption {
      default = pkgs.flood;
      type = types.package;
    };

    user = mkOption {
      type = types.str;
      default = "flood";
    };

    group = mkOption {
      type = types.str;
      default = "flood";
    };

    host = mkOption {
      type = types.str;
      default = "0.0.0.0";
    };

    port = mkOption {
      type = types.port;
      default = 3333;
    };

    baseUri = mkOption {
      type = types.str;
      default = "/";
    };

    useAuth = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to use Flood's default auth system or none";
    };

    webPassword = mkOption {
      type = types.str;
      default = "admin";
    };

    extraPackages = mkOption {
      type = types.listOf types.package;
      default = with pkgs; [
        mediainfo
      ];
    };

    backends = {
      transmission = {
        enable = mkEnableOption "Transmission torrent backend";

        settings = mkOption {
          type = types.attrs;
          default = tranmissionDefaults;
        };
      };

      rtorrent = {
        enable = mkEnableOption "rTorrent torrent backend";

        settings = mkOption {
          type = types.attrs;
          default = rtorrentDefaults;
        };
      };
    };

    mainDir = mkOption {
      type = types.path;
      default = "/drives/Mars";
    };
  };

  config = lib.mkIf cfg.enable {
    users = {
      groups = mkIf (cfg.group == "flood") {
        flood = {};
      };
      users = mkIf (cfg.user == "flood") {
        flood = {
          group = cfg.group;
          isSystemUser = true;
          extraGroups = ["wheel"];
          home = "/home/flood";
          createHome = true;
          homeMode = "777";
        };
        "${config.services.nginx.user}".extraGroups = [cfg.group];
      };
    };

    systemd.services.Flood = {
      description = "Flood torrent WebUI";

      after = ["network.target"];
      wantedBy = ["multi-user.target"];

      path = ["/run/current-system/sw/bin" cfg.package] ++ cfg.extraPackages;
      script =
        "${cfg.package}/bin/flood --host=\"${cfg.host}\" --baseuri=\"${cfg.baseUri}\" --port=${toString cfg.port}"
        + " --auth=\"${
          if cfg.useAuth
          then "default"
          else "none"
        }\""
        + optionalString (!cfg.useAuth) (
          optionalString cfg.backends.transmission.enable (
            " --trurl=\"http://${torrentHost}:${toString config.services.transmission.settings.rpc-port}/transmission/rpc\""
            + " --truser=\"${config.services.transmission.settings.rpc-user}\""
            + " --trpass=\"${config.services.transmission.settings.rpc-password}\""
          )
          + optionalString cfg.backends.rtorrent.enable (
            " --rtsocket=\"${config.services.rtorrent.rpcSocket}\""
            # + " --rthost=\"${torrentHost}\" --rtport=${toString config.services.rtorrent.port}"
          )
        );

      serviceConfig = {
        Type = "simple";
        User = cfg.user;
        Group = cfg.group;
        WorkingDirectory = cfg.mainDir;
        Restart = "on-failure";
        RestartSec = 3;
      };
    };

    services.transmission =
      if cfg.backends.transmission.enable
      then
        lib.mkMerge [
          tranmissionDefaults
          cfg.backends.transmission.settings
          {enable = true;}
        ]
      else {};

    services.rtorrent =
      if cfg.backends.rtorrent.enable
      then
        lib.mkMerge [
          rtorrentDefaults
          cfg.backends.rtorrent.settings
          {enable = true;}
        ]
      else {};
  };
}
