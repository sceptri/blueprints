# INFO: Setup VPN connection for Network Manager per
# https://discourse.nixos.org/t/setting-openvpn-connection-declaratively-switching-on-off-through-gnome-network-manager/22387/4
{
  pkgs,
  config,
  lib,
  ...
}: let
  rootDir = ../.;
  args = {};

  pathTo = type: (name: (rootDir + "/${type}/." + "/${name}.nix"));
  importConfig = type: (name: (args: (import (pathTo type name) args)));
  secrets = importConfig "secrets" "general" args;

  # TODO: switch to EduVPN (config expires 2025-03-14)
  MUNI_EDU_VPN = (pkgs.formats.ini {}).generate "MUNI_EDU_VPN.nmconnection" {
    connection = {
      id = "MUNI (EduVPN)";
      autoconnect = false;
      type = "vpn";
      uuid = "214dd9ed-0127-49da-aa3e-8c5533612b8d";
      permissions = "user:sceptri:;";
    };

    ipv4 = {
      method = "auto";
    };

    ipv6 = {
      addr-gen-mode = "default";
      method = "auto";
    };

    vpn = {
      ca = "/home/sceptri/.cert/nm-openvpn/eduvpn.muni.cz_student-sci_20241014_eduvpn_to_openvpn-ca.pem";
      cert = "/home/sceptri/.cert/nm-openvpn/eduvpn.muni.cz_student-sci_20241014_eduvpn_to_openvpn-cert.pem";
      cert-pass-flags = 0;
      challenge-response-flags = 2;
      connect-timeout = 10;
      connection-type = "tls";
      dev = "tun";
      key = "/home/sceptri/.cert/nm-openvpn/eduvpn.muni.cz_student-sci_20241014_eduvpn_to_openvpn-key.pem";
      remote = "147.251.205.38:443:tcp";
      remote-cert-tls = "server";
      reneg-seconds = 0;
      tls-crypt = "/home/sceptri/.cert/nm-openvpn/eduvpn.muni.cz_student-sci_20241014_eduvpn_to_openvpn-tls-crypt.pem";
      service-type = "org.freedesktop.NetworkManager.openvpn";
      tls-version-min = "1.3";
    };

    proxy = {};
  };

  MUNI = (pkgs.formats.ini {}).generate "MUNI.nmconnection" {
    connection = {
      id = "MUNI";
      autoconnect = false;
      type = "vpn";
      uuid = "2bb65058-da4a-4383-b482-cbeda858a43f";
      permissions = "user:sceptri:;";
    };

    ipv4 = {
      method = "auto";
    };

    ipv6 = {
      addr-gen-mode = "default";
      method = "auto";
    };

    vpn = {
      allow-pull-fqdn = "yes";
      ca = "/home/sceptri/.cert/nm-openvpn/muni-main-linux-ca.pem";
      connection-type = "password";
      dev = "tap";
      password-flags = 0;
      push-peer-info = "yes";
      remote = "vpn.ics.muni.cz:1194:udp";
      remote-random = "yes";
      username = "505579";
      verify-x509-name = "name:vpn.ics.muni.cz";
      service-type = "org.freedesktop.NetworkManager.openvpn";
    };

    proxy = {};
  };

  FI_MUNI = (pkgs.formats.ini {}).generate "FI_MUNI.nmconnection" {
    connection = {
      id = "FI MUNI";
      autoconnect = false;
      type = "vpn";
      uuid = "e0b0b5b0-400a-4b2b-9962-a4eae5572c02";
      permissions = "user:sceptri:;";
    };

    ipv4 = {
      method = "auto";
    };

    ipv6 = {
      addr-gen-mode = "default";
      method = "auto";
    };

    vpn = {
      allow-pull-fqdn = "yes";
      ca = "/home/sceptri/.cert/nm-openvpn/VPN FI MU-ca.pem";
      challenge-response-flags = 2;
      cipher = "AES-256-CBC";
      connection-type = "password";
      dev = "tun-fi";
      password-flags = 1;
      push-peer-info = "yes";
      remote = "vpn.fi.muni.cz:1194:udp";
      reneg-seconds = 86400;
      username = "x505579";
      verify-x509-name = "name:vpn.fi.muni.cz";
      service-type = "org.freedesktop.NetworkManager.openvpn";
      tls-crypt = "/home/sceptri/.cert/nm-openvpn/VPN FI MU-tls-crypt.pem";
    };

    proxy = {};
  };

  eduroam = (pkgs.formats.ini {}).generate "eduroam.nmconnection" {
    connection = {
      id = "eduroam";
      uuid = "34af0d61-629f-4161-9502-1089b5a44d46";
      type = "wifi";
      interface-name = "wlp0s20f3";
      timestamp = "1663575915";
    };

    wifi = {
      mode = "infrastructure";
      ssid = "eduroam";
    };

    wifi-security = {
      key-mgmt = "wpa-eap";
    };

    "802-1x" = {
      ca-cert = "/home/sceptri/Documents/Other/Eduroam/ca.pem";
      eap = "peap";
      identity = "505579@muni.cz";
      password = secrets.wifi.eduroam;
      phase2-auth = "mschapv2";
    };

    ipv4 = {
      method = "auto";
    };

    ipv6 = {
      addr-gen-mode = "stable-privacy";
      method = "auto";
    };

    proxy = {};
  };
in {
  # "NetworkManager/system-connections/${MUNI.name}" = {
  #   source = MUNI;
  #   mode = "0600";
  # };

  "NetworkManager/system-connections/${MUNI_EDU_VPN.name}" = {
    source = MUNI_EDU_VPN;
    mode = "0600";
  };

  "NetworkManager/system-connections/${FI_MUNI.name}" = {
    source = FI_MUNI;
    mode = "0600";
  };

  "NetworkManager/system-connections/${eduroam.name}" = {
    source = eduroam;
    mode = "0600";
  };
}
